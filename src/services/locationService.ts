

import { Diagnostic } from '@ionic-native/diagnostic';
import { Geolocation } from '@ionic-native/geolocation';
import { Platform } from 'ionic-angular/platform/platform';
import { Observable, Observer } from 'rxjs';
import { Injectable } from '@angular/core';
import { AlertController, ModalController, App, LoadingController } from 'ionic-angular';
@Injectable()

export class locationService {

    constructor(public alertCtrl: AlertController,
        public diagnostic: Diagnostic,
        public modalController: ModalController,
        public loadingCtrl: LoadingController,
        public platform: Platform,
        public geolocation: Geolocation,
        protected app: App) {

            
    }

    getUserLocation() {
        return new Observable((observer: Observer<boolean>) => {
            var loading = this.loadingCtrl.create({
                content: 'Retrieving Location....'
            });

            loading.present();

            this.geolocation.getCurrentPosition().then((position) => {
                loading.dismiss();
                localStorage.setItem("LocationPreference", "GPS");
                localStorage.setItem('Latitude', position.coords.latitude.toString());
                localStorage.setItem('Longitude', position.coords.longitude.toString());
                localStorage.setItem("AppConfigured" , "True");
                observer.next(true);
            }).catch((error) => {
                loading.dismiss();
                console.log('Error getting location', error);
                observer.error(false);
            });
            
        });
    }

    isDeviceLocationEnabled() {
        return new Observable((observer: Observer<boolean>) => {
            this.diagnostic.isLocationEnabled().then(
                (isAvailable) => {
                    switch (isAvailable) {
                        case true:
                            observer.next(true);
                            break;
                        case false:
                            observer.error(false);
                            break;
                        default:
                            break;
                    }
                })
                .catch((error) => {
                    observer.error(false);
                });
        });
    }

    isLocationAuthorized(){
        return new Observable((observer: Observer<boolean>) => {
            this.diagnostic.requestLocationAuthorization("when_in_use").then(response => {
                switch (response) {
                    case "GRANTED":
                        observer.next(true);
                        break;
                    case "DENIED":
                        observer.error(false);
                        break;
                    default:
                        break;
                }
            })
            .catch(error => {
                observer.error(false);
            });
        });
    }

    getGpsAvailability(){
        return new Observable((observer: Observer<boolean>) => {
            this.diagnostic.isLocationAvailable().then(response => {
                switch (response) {
                    case true:
                        observer.next(true);
                        break;
                    case false:
                        observer.error(false);
                        break;
                    default:
                        break;
                }
            })
            .catch(error => {
                observer.error(false);
            });
        });
    }
}