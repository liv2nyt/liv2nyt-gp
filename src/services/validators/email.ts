import { FormControl } from '@angular/forms';
 
export class EmailValidator {
 
    static isEmaillValid(control: FormControl): any {
 
        if(control.value.indexOf("@") == -1){
            return {
                "Invalid Email Address": true
            };
        }
 
        return null;
    }
 
}