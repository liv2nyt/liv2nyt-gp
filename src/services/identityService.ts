import { Injectable } from '@angular/core';
import { Events, LoadingController, AlertController } from 'ionic-angular';
import { UserProvider } from '../providers/userProvider';
import { TabsPage } from '../pages/tabs/tabs';
import { LocationModal } from '../pages/locationModal/locationModal';
import { SpotDistance } from '../contracts/contracts';

@Injectable()

export class identityService {
        distanceRadius = 100;

        constructor(public events: Events,
                public loadingCtrl: LoadingController,
                public alertCtrl: AlertController,
                public locationModal: LocationModal,
                public userProvider: UserProvider) {
                var radius = localStorage.getItem("DistanceRadius");
                if (radius != undefined && radius != null) {
                        this.distanceRadius = parseInt(radius);
                }
        }

        authenticateLogin(loginDetails, initRegister) {

                let loading = this.loadingCtrl.create({
                        content: 'Logging in ...'
                });

                loading.present();
                this.userProvider.authenticateUser(loginDetails).then((response: any) => {
                        if (response.WasSuccessful) {
                                debugger;
                                localStorage.setItem('password', loginDetails.Password);
                                localStorage.setItem('email', response.Data.EmailAddress);
                                localStorage.setItem('userId', response.Data.UserId.toString());
                                localStorage.setItem('gender', response.Data.Gender);
                                localStorage.setItem('name', response.Data.FulName);
                                localStorage.setItem('username', loginDetails.Username);
                                localStorage.setItem('mobileNumber', response.Data.MobileNumber);
                                localStorage.setItem('userAuthToken', response.Data.Token);
                                localStorage.setItem('profilePic', response.Data.Image);
                                localStorage.setItem('profilePicTiny', response.Data.TinyImage);
                                localStorage.setItem('hasLoggedIn', '1');
                                loading.dismiss();

                                if (initRegister) {
                                        //when walkthrough is completed
                                        //this.events.publish('RouteBroadcastRoot', 'walkthrough');
                                        this.events.publish("RegisterComplete")
                                } else {
                                        this.checkLocationSettings();
                                }

                        }
                        else {
                                loading.dismiss();
                                this.showErrorMessage(response);
                        }

                })
                        .catch(err => {
                                loading.dismiss();
                                this.showErrorMessage(err);

                        });
        }

        IsJsonString(str) {
                try {
                        JSON.parse(str);
                } catch (e) {
                        return false;
                }
                return true;
        }

        showErrorMessage(message) {



                if (message.Heading != undefined) {
                        this.events.publish('ShowErrorAlert', message.Heading, message.Description);
                } else if (message._body != undefined && this.IsJsonString(message._body)) {
                        message = JSON.parse(message._body);
                        this.events.publish('ShowErrorAlert', message.Heading, message.Description);
                } else {
                        console.error(message.toString());
                        this.events.publish('ShowErrorAlert', "An error occured", "An error occured, please ensure you have a internet connection and try again");
                }
        }

        registerUser(registrationDetails) {

                let loading = this.loadingCtrl.create({
                        content: 'Registering New User ...'
                });

                loading.present();
                this.userProvider.registerUser(registrationDetails).then((response: any) => {

                        if (response.WasSuccessful) {

                                loading.dismiss();
                                var loginDetails = {
                                        Username: registrationDetails.username,
                                        Password: registrationDetails.hashedPassword,
                                }

                                this.authenticateLogin(loginDetails, true);

                        }
                        else {
                                loading.dismiss();
                                this.showErrorMessage(response);
                        }

                })
                        .catch(err => {
                                loading.dismiss();
                                this.showErrorMessage(err)
                        });
        }

        updateProfile(profileDetails) {
                let loading = this.loadingCtrl.create({
                        content: 'Updating Profile ...'
                });
                loading.present();

                if (profileDetails.FullName != undefined) {
                        localStorage.setItem('name', profileDetails.FullName);
                }
                if (profileDetails.Gender != undefined) {
                        localStorage.setItem('gender', profileDetails.Gender);
                }
                if (profileDetails.CellNumber != undefined) {
                        localStorage.setItem('mobileNumber', profileDetails.CellNumber);
                }
                if (profileDetails.EmailAddress != undefined) {
                        localStorage.setItem('email', profileDetails.EmailAddress);
                }

                this.userProvider.updateUserCall(profileDetails).then((response: any) => {
                        this.events.publish("updateProfile");
                        loading.dismiss();
                })
                        .catch(err => {
                                loading.dismiss();
                                this.showErrorMessage(err)
                        });
        }

        registerExternalUser(registrationDetails) {

                let loading = this.loadingCtrl.create({
                        content: 'Registering New User ...'
                });

                loading.present();
                this.userProvider.registerExternalUser(registrationDetails).then((response: any) => {

                        if (response.WasSuccessful) {

                                loading.dismiss();
                                var loginDetails = {
                                        EmailAddress: registrationDetails.EmailAddress,
                                        FacebookId: registrationDetails.FacebookId,
                                }

                                this.loginExternal(loginDetails, true);

                        }
                        else {
                                loading.dismiss();
                                this.showErrorMessage(response);
                        }

                })
                        .catch(err => {
                                loading.dismiss();
                                this.showErrorMessage(err)
                        });
        }

        loadSpots(page) {

                let loading = this.loadingCtrl.create({
                        content: 'Loading Spots...'
                });

                loading.present();

                var spotDistance = new SpotDistance();
                spotDistance.Page = page;
                spotDistance.Radius = this.distanceRadius;
                spotDistance.Latitude = +localStorage.getItem('Latitude');
                spotDistance.Longitude = +localStorage.getItem('Longitude');

                this.userProvider.loadSpotsCall(spotDistance).then((response: any) => {
                })
                        .catch(err => {
                                loading.dismiss();
                                this.showErrorMessage(err)
                        });

        }

        loginExternal(loginExternalDetails,initLogin) {

                let loading = this.loadingCtrl.create({
                        content: 'Authenticating via Facebook...'
                });

                loading.present();


                this.userProvider.SignInExternalUser({ EmailAddress: loginExternalDetails.EmailAddress, FacebookId: loginExternalDetails.FacebookId }).then((response: any) => {
                        if (response.WasSuccessful) {

                                localStorage.setItem('loginType', "Facebook");
                                localStorage.setItem('email', response.Data.EmailAddress);
                                localStorage.setItem('userId', response.Data.UserId.toString());
                                localStorage.setItem('gender', response.Data.Gender);
                                localStorage.setItem('username', response.Data.Username);
                                localStorage.setItem('name', response.Data.FullName);
                                localStorage.setItem('mobileNumber', response.Data.MobileNumber);
                                localStorage.setItem('userAuthToken', response.Data.Token);
                                localStorage.setItem('hasLoggedIn', '1');
                                localStorage.setItem('profilePic', response.Data.Image);
                                localStorage.setItem('profilePicTiny', response.Data.TinyImage);
                                localStorage.setItem('FacebookExternalId', loginExternalDetails.FacebookId);

                                loading.dismiss();
                                if(initLogin != undefined && initLogin == true){
                                        this.events.publish("RegisterCompleteExternal")
                                }else{
                                        this.checkLocationSettings();
                                }
                                

                        }
                        else {

                                loading.dismiss();
                                this.events.publish('RouteBroadcastPush', 'facebookRegistration');


                        }
                })
                        .catch(err => {
                                loading.dismiss();
                                console.error("Error connecting to liv2nyt facebook call: " + err.toString());
                                this.events.publish('RouteBroadcastPush', 'facebookRegistration');
                        });

        }

        checkLocationSettings() {
                var locationPreference = localStorage.getItem("LocationPreference");

                if (locationPreference == null || locationPreference == "") {
                        this.events.publish('RouteBroadcastRoot', 'LocationModal');
                }

                switch (locationPreference) {
                        case "GPS":
                                //this.locationModal.gpsFirstRun();
                                this.locationModal.gpsFirstRun();
                                //this.navCtrl.setRoot(HomePage);
                                break;
                        case "Address":
                                var latitude = localStorage.getItem("Latitude");

                                if (latitude != null || latitude != "") {
                                        var address = localStorage.getItem("Address");
                                        if (address != null) {
                                                this.events.publish('showToast', "Viewing events from: " + address, true);
                                        }
                                        this.events.publish('RouteBroadcastRoot', 'TabsPage');
                                }
                                else {
                                        this.events.publish('RouteBroadcastRoot', 'LocationModal');
                                }
                                break;
                        default:
                                console.log("Unknown Error");
                                break;
                }
        }
}