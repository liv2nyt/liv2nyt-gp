import { Injectable } from '@angular/core';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { Crop } from '@ionic-native/crop';
import { Headers } from '@angular/http';
import { ToastController, Platform, LoadingController, AlertController, Events } from 'ionic-angular';

@Injectable()

export class ImageUploader {

    public options: CameraOptions = {
        allowEdit: true,
        sourceType: this.camera.PictureSourceType.SAVEDPHOTOALBUM,
        mediaType: this.camera.MediaType.ALLMEDIA,
        destinationType: this.camera.DestinationType.FILE_URI
    }
    file: any = "";
    headers: any;

    constructor(public toastCtrl: ToastController,
        private camera: Camera,
        private transfer: FileTransfer,
        private crop: Crop,
        public platform: Platform,
        public loadingCtrl: LoadingController,
        public alertCtrl: AlertController,
        public events: Events) {

    }


    uploadImage(imageUrl, status): Promise<any> {

        return this.camera.getPicture(this.options)
            .then((fileUri) => {
                // Crop Image, on android this returns something like, '/storage/emulated/0/Android/...'
                // Only giving an android example as ionic-native camera has built in cropping ability
                if (this.platform.is('ios')) {
                    return fileUri
                } else if (this.platform.is('android')) {
                    // Modify fileUri format, may not always be necessary
                    fileUri = 'file://' + fileUri;
                    /* Using cordova-plugin-crop starts here */
                    return this.crop.crop(fileUri, { quality: 100 });
                }
            })
            .then((path) => {
                // path looks like 'file:///storage/emulated/0/Android/data/com.foo.bar/cache/1477008080626-cropped.jpg?1477008106566'
                console.log('Cropped Image Path!: ' + path);
                this.file = path;
                return path;
            })
            .then(() => {
                debugger;
                var loading = this.loadingCtrl.create({
                    content: 'Uploading Image ...'
                });

                loading.present();
                // Destination URL
                var url = imageUrl;

                // File for Upload
                var temp = this.file
                var targetPath = temp.substring(0, temp.lastIndexOf("?"));

                // File name only
                var filename = targetPath.substring(this.file.lastIndexOf("/") + 1, this.file.length + 1);

                this.headers = new Headers();
                var authToken = localStorage.getItem('userAuthToken');

                this.headers.append('Authorization', "Bearer " + authToken);

                var options: FileUploadOptions = {
                    fileKey: "file",
                    fileName: filename,
                    headers: this.headers
                };

                const fileTransfer: FileTransferObject = this.transfer.create();

                // Use the FileTransfer to upload the image
                fileTransfer.upload(targetPath, url, options, true).then((data: any) => {
                    var result = JSON.parse(data.response);
                    var imageUrl = result.Data;

                    this.events.publish("showToast", "Image Uploaded Successfully", true);

                    if(status == "profile"){
                        localStorage.setItem('profilePic', imageUrl.Image);
                        localStorage.setItem('profilePicTiny', imageUrl.TinyImage);
                        this.events.publish("imageUploaded");
                    }else if(status == "venue"){
                        this.events.publish("venueImageUploaded",imageUrl.Image );
                    }
                    

                    loading.dismiss();
                    return imageUrl;
                }, err => {
                    loading.dismiss()
                    console.error("Error uploading file : " + err.toString());
                    this.events.publish('ShowErrorAlert', 'Failed to Upload Image', "The image failed to upload, please ensure you have an internet conenction and try again");
                });

            })

    }

}