import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { identityService } from '../services/identityService';
import { SettingsPage } from '../pages/settings/settings';
import { LinkUpPage } from '../pages/linkup/linkup';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { Http, XHRBackend, RequestOptions } from '@angular/http';
import { RegistrationPage } from '../pages/registration/registration';
import { FacebookRegistrationPage } from '../pages/facebookRegistration/facebookRegistration';
import { LandingPage } from '../pages/landing/landing';
import { LoginPage } from '../pages/login/login';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { UserProvider } from '../providers/userProvider';
import { HttpModule }      from '@angular/http';
import { Facebook } from '@ionic-native/facebook';
import { Crop } from '@ionic-native/crop';
import { Camera } from '@ionic-native/camera';
import { FileTransfer } from '@ionic-native/file-transfer';
import { ImageUploader } from '../services/imageUploader';
import { Base64 } from '@ionic-native/base64';
import { LocationModal } from '../pages/locationModal/locationModal';
import { Diagnostic } from '@ionic-native/diagnostic';
import { Geolocation } from '@ionic-native/geolocation';
import { Keyboard } from '@ionic-native/keyboard';
import { locationService } from '../Services/locationService';
import { SpotPage } from '../pages/spot/spot';
import { ElasticHeaderDirective } from '../directives/elastic-header/elastic-header';
import { LazyLoadImageModule } from 'ng2-lazyload-image';
import { AuthInterceptor } from '../providers/AuthInterceptor';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { LaunchNavigator } from '@ionic-native/launch-navigator';
import { SocialSharing } from '@ionic-native/social-sharing';
import { FriendsPage } from '../pages/friends/friends';
import { FriendsProfile } from '../pages/friends/FriendsProfile/FriendsProfile';
import { FriendsModal } from '../pages/friends/FriendsModal/FriendsModal';
import { CreateVenuePage } from '../pages/createVenue/createVenue';
import { InvitationsPage } from '../pages/invitations/invitations';

@NgModule({
  declarations: [
    MyApp,
    LinkUpPage,
    SettingsPage,
    HomePage,
    TabsPage,
    LandingPage,
    RegistrationPage,
    LoginPage,
    FacebookRegistrationPage,
    LocationModal,
    SpotPage,
    ElasticHeaderDirective,
    FriendsPage,
    FriendsProfile,
    FriendsModal,
    CreateVenuePage,
    InvitationsPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    LazyLoadImageModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    LinkUpPage,
    SettingsPage,
    HomePage,
    TabsPage,
    LandingPage,
    RegistrationPage,
    LoginPage,
    FacebookRegistrationPage,
    LocationModal,
    SpotPage,
    FriendsPage,
    FriendsProfile,
    FriendsModal,
    CreateVenuePage,
    InvitationsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    identityService,
    UserProvider,
    Facebook,
    ImageUploader,
    Camera,
    Crop,
    FileTransfer,
    Base64,
    InAppBrowser,
    Diagnostic,
    Geolocation,
    Keyboard,
    SocialSharing,
    LaunchNavigator,
    locationService,
    LocationModal,
    {provide: Http, useFactory: authFunction, deps: [XHRBackend, RequestOptions]}, 
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}

export function authFunction(backend:XHRBackend, defaultOptions:RequestOptions) {
  return new AuthInterceptor(backend, defaultOptions);
}
