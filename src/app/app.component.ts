import { Platform, Events, AlertController, Nav } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Component, ViewChild, ElementRef, Renderer } from '@angular/core';
import { RegistrationPage } from '../pages/registration/registration';
import { LandingPage } from '../pages/landing/landing';
import { TabsPage } from '../pages/tabs/tabs';
import { FacebookRegistrationPage } from '../pages/facebookRegistration/facebookRegistration';
import { LoginPage } from '../pages/login/login';
import { LocationModal } from '../pages/locationModal/locationModal';
import { SpotPage } from '../pages/spot/spot';
import { FriendsPage } from '../pages/friends/friends';
import { FriendsProfile } from '../pages/friends/FriendsProfile/FriendsProfile';
import { CreateVenuePage } from '../pages/createVenue/createVenue';
import { InvitationsPage } from '../pages/invitations/invitations';

@Component({
        templateUrl: 'app.html'
})
export class MyApp {
        rootPage: any = LandingPage;
        @ViewChild(Nav) nav: Nav;
        @ViewChild('toasty') toasty: ElementRef;
        notification: any = { title: ' ', message: ' ', success: true };

        constructor(public renderer: Renderer, platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, public events: Events, public alertCtrl: AlertController) {
                platform.ready().then(() => {
                        statusBar.styleDefault();
                        this.initializeApp(splashScreen);
                });

                this.events.subscribe('ShowErrorAlert', (Heading, description) => {

                        const alert = this.alertCtrl.create({
                                title: Heading,
                                subTitle: description,
                                buttons: ['OK']
                        });

                        alert.present();

                });

                this.events.subscribe('RouteBroadcastRoot', (page) => {
                        if (page == "TabsPage") {
                                this.nav.setRoot(TabsPage);
                        } else if (page == "LocationModal") {
                                this.nav.setRoot(LocationModal);
                        }
                });

                this.events.subscribe('RouteBroadcastPush', (page) => {
                        if (page == "facebookRegistration") {
                                this.nav.push(FacebookRegistrationPage);
                        } else if (page == "SpotPage") {
                                this.nav.push(SpotPage);
                        } else if (page == "FriendsPage") {
                                this.nav.push(FriendsPage);
                        }


                });

                this.events.subscribe('openSpot', (venueId) => {
                        this.nav.push(SpotPage, { venueId: venueId });

                });

                this.events.subscribe('openProfile', (userId) => {
                        this.nav.push(FriendsProfile, { userId: userId });
                });

                this.events.subscribe('openLocationSettings', () => {
                        this.nav.push(LocationModal);
                });

                this.events.subscribe('openCreateVenue', () => {
                        this.nav.push(CreateVenuePage);
                });

                this.events.subscribe('popNav', () => {
                        this.nav.pop();
                });

                this.events.subscribe('showToast', (message, status) => {
                        this.showToast(message, status);
                });

                this.events.subscribe('invitations', () => {
                        this.nav.push(InvitationsPage);

                });
        }


        initializeApp(splashScreen) {

                if (localStorage.getItem('hasLoggedIn') != undefined && localStorage.getItem('hasLoggedIn') == '1') {
                        this.rootPage = TabsPage;
                        splashScreen.hide();
                } else {
                        this.rootPage = LandingPage;
                        splashScreen.hide();
                }

        }

        showToast(message, status) {
                //this.notification.title = message.Heading;
                this.notification.message = message;
                this.notification.success = status;

                setTimeout(() => {
                        this.renderer.setElementClass(this.toasty.nativeElement, 'enter', true);
                }, 1000);
                this.hideToast();
        }

        hideToast() {
                setTimeout(() => {
                        this.renderer.setElementClass(this.toasty.nativeElement, 'enter', false);
                }, 5000);
        }

        


}
