import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import 'rxjs/Rx';
import 'rxjs/add/operator/map';
import { ServiceResult, VenueType } from '../contracts/contracts';

@Injectable()
export class UserProvider {
        url: string;
        headers: any;

        constructor(public http: Http) {

                //this.url = 'http://localhost:46362/api';
                this.url = 'http://linkapi.liv2nyt.com/api';
                this.headers = new Headers();
                //var authToken = localStorage.getItem('userAuthToken');
                this.headers.append('Content-Type', 'application/json; charset=utf-8');
                //this.headers.append('Authorization', "Bearer " + authToken);
        }

        authenticateUser(loginUserDetails) {

                var json = JSON.stringify(loginUserDetails);
                var params = json;

                return new Promise((resolve, reject) => {
                        this.http.post(this.url + '/Login/Username', params, {

                                headers: this.headers
                        })
                                .map((response: Response) => {
                                        var result = <any>response.json();
                                        return result;
                                })
                                .subscribe(data => {

                                        resolve(data);
                                },
                                        error => {

                                                reject(error);
                                        });
                });
        }

        getAddressPredictions(searchTerm) {
                var headerss = new Headers();
                headerss.append('responseType', 'arraybuffer');

                return new Promise((resolve, reject) => {

                        this.http.get(this.url + '/Event/AddressPredictions/' + searchTerm)
                                .map((response: Response) => {
                                        var res = response.json();
                                        return res;
                                })
                                .subscribe(data => {
                                        resolve(data);
                                }, error => {
                                        reject(error);
                                });
                });

        }

        searchFreinds(searchTerm) {
                var headerss = new Headers();
                headerss.append('responseType', 'arraybuffer');
                searchTerm = "?searchTerm=" + searchTerm;
                return new Promise((resolve, reject) => {

                        this.http.get(this.url + '/Friend/Search' + searchTerm)
                                .map((response: Response) => {
                                        var res = response.json();
                                        return res;
                                })
                                .subscribe(data => {
                                        resolve(data);
                                }, error => {
                                        reject(error);
                                });
                });

        }

        getAllFriends() {

                return new Promise((resolve, reject) => {

                        this.http.get(this.url + '/Friend/All')
                                .map((response: Response) => {
                                        var res = response.json();
                                        return res;
                                })
                                .subscribe(data => {
                                        resolve(data);
                                }, error => {
                                        reject(error);
                                });
                });

        }

        loadProfile(userId) {

                return new Promise((resolve, reject) => {

                        this.http.get(this.url + '/User/' + userId)
                                .map((response: Response) => {
                                        var res = response.json();
                                        return res;
                                })
                                .subscribe(data => {
                                        resolve(data);
                                }, error => {
                                        reject(error);
                                });
                });

        }

        getAllFriendRequests() {

                return new Promise((resolve, reject) => {

                        this.http.get(this.url + '/Friend/Requests')
                                .map((response: Response) => {
                                        var res = response.json();
                                        return res;
                                })
                                .subscribe(data => {
                                        resolve(data);
                                }, error => {
                                        reject(error);
                                });
                });

        }

        getFriendsFeed() {
                return new Promise((resolve, reject) => {

                        this.http.get(this.url + '/Friend/All')
                                .map((response: Response) => {
                                        var res = response.json();
                                        return res;
                                })
                                .subscribe(data => {
                                        resolve(data);
                                }, error => {
                                        reject(error);
                                });
                });

        }

        postFriendsFeed(friendFeedRequest) {
                var jsonDataForm = JSON.stringify(friendFeedRequest);

                return new Promise((resolve, reject) => {

                        this.http.post(this.url + '/Venue/FriendsDistance', jsonDataForm, {

                                headers: this.headers
                        })
                                .map((response: Response) => {
                                        var result = <any>response.json();
                                        return result;
                                })
                                .subscribe(data => {
                                        resolve(data);
                                },
                                        error => {
                                                reject(error);
                                        });
                });

        }

        getLatLong(address) {

                return new Promise((resolve, reject) => {

                        this.http.get(this.url + '/Event/GetCoordinates/' + address)
                                .map((response: Response) => {
                                        var res = response.json();
                                        return res;
                                })
                                .subscribe(data => {
                                        resolve(data);
                                }, error => {
                                        reject(error);
                                });
                });

        }

        registerUser(registerUserDetails) {

                var json = JSON.stringify(registerUserDetails);
                var params = json;

                return new Promise((resolve, reject) => {
                        this.http.post(this.url + '/User/Register', params, {

                                headers: this.headers
                        })
                                .map((response: Response) => {
                                        var result = <any>response.json();
                                        return result;
                                })
                                .subscribe(data => {

                                        resolve(data);
                                },
                                        error => {

                                                reject(error);
                                        });
                });
        }

        createVenue(venueDetails) {

                var json = JSON.stringify(venueDetails);
                var params = json;

                return new Promise((resolve, reject) => {
                        this.http.post(this.url + '/Venue', params, {

                                headers: this.headers
                        })
                                .map((response: Response) => {
                                        var result = <any>response.json();
                                        return result;
                                })
                                .subscribe(data => {

                                        resolve(data);
                                },
                                        error => {

                                                reject(error);
                                        });
                });
        }

        addFriend(userId) {

                var params = "{UserId:" + userId + "}";

                return new Promise((resolve, reject) => {
                        this.http.post(this.url + '/Friend/Add', params, {

                                headers: this.headers
                        })
                                .map((response: Response) => {
                                        var result = <any>response.json();
                                        return result;
                                })
                                .subscribe(data => {

                                        resolve(data);
                                },
                                        error => {

                                                reject(error);
                                        });
                });
        }

        declineFriend(userId) {

                var params = "{UserId:" + userId + "}";

                return new Promise((resolve, reject) => {
                        this.http.post(this.url + '/Friend/Decline', params, {

                                headers: this.headers
                        })
                                .map((response: Response) => {
                                        var result = <any>response.json();
                                        return result;
                                })
                                .subscribe(data => {

                                        resolve(data);
                                },
                                        error => {

                                                reject(error);
                                        });
                });
        }

        confirmFriend(userId) {

                var params = "{UserId:" + userId + "}";

                return new Promise((resolve, reject) => {
                        this.http.post(this.url + '/Friend/Confirm', params, {

                                headers: this.headers
                        })
                                .map((response: Response) => {
                                        var result = <any>response.json();
                                        return result;
                                })
                                .subscribe(data => {

                                        resolve(data);
                                },
                                        error => {

                                                reject(error);
                                        });
                });
        }

        updateUserCall(profileDetails) {

                var json = JSON.stringify(profileDetails);
                var params = json;

                return new Promise((resolve, reject) => {
                        this.http.put(this.url + '/User', params, {

                                headers: this.headers
                        })
                                .map((response: Response) => {
                                        var result = <any>response.json();
                                        return result;
                                })
                                .subscribe(data => {

                                        resolve(data);
                                },
                                        error => {

                                                reject(error);
                                        });
                });
        }

        registerExternalUser(registerUserDetails) {

                var json = JSON.stringify(registerUserDetails);
                var params = json;

                return new Promise((resolve, reject) => {
                        this.http.post(this.url + '/User/Register/Facebook', params, {

                                headers: this.headers
                        })
                                .map((response: Response) => {
                                        var result = <any>response.json();
                                        return result;
                                })
                                .subscribe(data => {

                                        resolve(data);
                                },
                                        error => {

                                                reject(error);
                                        });
                });
        }

        SignInExternalUser(loginUserDetails) {
                var jsonDataForm = JSON.stringify(loginUserDetails);

                return new Promise((resolve, reject) => {
                        this.http.post(this.url + '/Login/Facebook', jsonDataForm, {

                                headers: this.headers
                        })
                                .map((response: Response) => {
                                        var result = <any>response.json();
                                        return result;
                                })
                                .subscribe(data => {
                                        resolve(data);
                                },
                                        error => {
                                                reject(error);
                                        });
                });
        }

        loadSpotsCall(spotDistance) {
                var jsonDataForm = JSON.stringify(spotDistance);

                return new Promise((resolve, reject) => {
                        this.http.post(this.url + '/Venue/AllVenuesDistance', jsonDataForm, {

                                headers: this.headers
                        })
                                .map((response: Response) => {
                                        var result = <any>response.json();
                                        return result;
                                })
                                .subscribe(data => {
                                        resolve(data);
                                },
                                        error => {
                                                reject(error);
                                        });
                });
        }

        loadSpotCall(spotDistance) {
                var jsonDataForm = JSON.stringify(spotDistance);

                return new Promise((resolve, reject) => {
                        this.http.post(this.url + '/Venue/VenueDistance', jsonDataForm, {

                                headers: this.headers
                        })
                                .map((response: Response) => {
                                        var result = <any>response.json();
                                        return result;
                                })
                                .subscribe(data => {
                                        resolve(data);
                                },
                                        error => {
                                                reject(error);
                                        });
                });
        }

        checkIn(checkInRequest) {
                var jsonDataForm = JSON.stringify(checkInRequest);

                debugger;
                return new Promise((resolve, reject) => {
                        this.http.post(this.url + '/Venue/CheckIn', jsonDataForm, {

                                headers: this.headers
                        })
                                .map((response: Response) => {
                                        var result = <any>response.json();
                                        return result;
                                })
                                .subscribe(data => {
                                        resolve(data);
                                },
                                        error => {
                                                reject(error);
                                        });
                });
        }

        checkOut(checkOutRequest) {
                var jsonDataForm = JSON.stringify(checkOutRequest);

                debugger;
                return new Promise((resolve, reject) => {
                        this.http.post(this.url + '/Venue/CancelCheckIn', jsonDataForm, {

                                headers: this.headers
                        })
                                .map((response: Response) => {
                                        var result = <any>response.json();
                                        return result;
                                })
                                .subscribe(data => {
                                        resolve(data);
                                },
                                        error => {
                                                reject(error);
                                        });
                });
        }

        getVenueTypes() {

                return new Promise((resolve, reject) => {

                        this.http.get(this.url + '/VenueType')
                                .map((response: Response) => {
                                        var res = response.json();
                                        return res;
                                })
                                .subscribe(data => {
                                        resolve(data);
                                }, error => {
                                        reject(error);
                                });
                });

        }

        initializeVenueTypes() {
                if (localStorage.getItem('venueTypes') != undefined) {
                        return JSON.parse(localStorage.getItem('venueTypes'));
                }
                else {
                        this.getVenueTypes().then((response: ServiceResult<VenueType[]>) => {
                                if (response.WasSuccessful) {
                                        localStorage.setItem('venueTypes', JSON.stringify(response.Data));
                                        return response.Data;
                                }
                        }).catch((error: ServiceResult<string>) => {

                        });
                }
        }

        SendInvitations(invitationRequest) {
                var jsonDataForm = JSON.stringify(invitationRequest);
                
                debugger;
                return new Promise((resolve, reject) => {
                        this.http.post(this.url + '/Invitation/Send', jsonDataForm, {

                                headers: this.headers
                        })
                                .map((response: Response) => {
                                        var result = <any>response.json();
                                        return result;
                                })
                                .subscribe(data => {
                                        resolve(data);
                                },
                                        error => {
                                                reject(error);
                                        });
                });
        }

        getAllFriendsWithInvites(venueId) {

                return new Promise((resolve, reject) => {

                        this.http.get(this.url + '/Friend/Invited/' + venueId)
                                .map((response: Response) => {
                                        var res = response.json();
                                        return res;
                                })
                                .subscribe(data => {
                                        resolve(data);
                                }, error => {
                                        reject(error);
                                });
                });

        }

        getInvitationsReceived() {

                return new Promise((resolve, reject) => {

                        this.http.get(this.url + '/Invitation/Received')
                                .map((response: Response) => {
                                        var res = response.json();
                                        return res;
                                })
                                .subscribe(data => {
                                        resolve(data);
                                }, error => {
                                        reject(error);
                                });
                });

        }

        getInvitationsSent() {

                return new Promise((resolve, reject) => {

                        this.http.get(this.url + '/Invitation/Sent')
                                .map((response: Response) => {
                                        var res = response.json();
                                        return res;
                                })
                                .subscribe(data => {
                                        resolve(data);
                                }, error => {
                                        reject(error);
                                });
                });

        }

        acceptInvitation(invitationId) {

                return new Promise((resolve, reject) => {

                        this.http.put(this.url + '/Invitation/Accept/' + invitationId, null, {

                                headers: this.headers
                        })
                                .map((response: Response) => {
                                        var res = response.json();
                                        return res;
                                })
                                .subscribe(data => {
                                        resolve(data);
                                }, error => {
                                        reject(error);
                                });
                });

        }

        declineInvitation(invitationId) {

                return new Promise((resolve, reject) => {

                        this.http.put(this.url + '/Invitation/Decline/' + invitationId, null, {

                                headers: this.headers
                        })
                                .map((response: Response) => {
                                        var res = response.json();
                                        return res;
                                })
                                .subscribe(data => {
                                        resolve(data);
                                }, error => {
                                        reject(error);
                                });
                });

        }
}