
import { Injectable } from '@angular/core';
import { Http, ConnectionBackend, RequestOptions, Request, Response, RequestOptionsArgs } from '@angular/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class AuthInterceptor extends Http {

    constructor(backend: ConnectionBackend, defaultOptions: RequestOptions) {
        super(backend, defaultOptions);
    }

    request(url: any | Request, options?: RequestOptionsArgs): Observable<Response> {
        var authToken = localStorage.getItem('userAuthToken');
        url.headers.append('Authorization', "Bearer " + authToken);

        console.log('My new request...');

        return super.request(url, options);
    }

}