import {Injectable} from '@angular/core';

@Injectable()


export class SpotDistance {
    Page: number;
    Latitude: number;
    Longitude: number;
    Radius: number;
}

export class ServiceResult<T> {
    WasSuccessful: boolean;
    Heading: string;
    Description: string;
    Data: T;
}

export class FriendsFeedRequest {
    Page: number;
    Latitude: number;
    Longitude: number;
}

export class FriendsVenuesDistanceResult {
    FriendsVenuesDistanceUser: FriendsVenuesDistanceUser;
    FriendsVenuesDistanceVenue: FriendsVenuesDistanceVenue;
    Distance: number;
}

export class FriendsVenuesDistanceUser {
    Id: number;
    FullName: string;
    Image: string;
    TinyImage: string;
}

export class FriendsVenuesDistanceVenue {
    Id: number;
    Name: string;
}

export class VenueType {
    Id: number;
    Description: string;
}

export class Time
{
    Open: string;
    Close: string;
}

export class Day
{
    DayOfWeek: number;

    Times: Time[];
}

export class Hours
{
    Days: Day[];
}

export class InvitationDetails
{
    UserList: string;
    VenueId: number;
}

export class AddFriend
{
    UserId: number;
}