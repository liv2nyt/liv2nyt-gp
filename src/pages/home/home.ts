import { Component, ViewChild } from '@angular/core';
import { NavController, Events, LoadingController, Refresher } from 'ionic-angular';
import { identityService } from '../../services/identityService';
import { SpotDistance, VenueType, ServiceResult } from '../../contracts/contracts';
import { UserProvider } from '../../providers/userProvider';

@Component({
    selector: 'page-home',
    templateUrl: 'home.html'
})
export class HomePage {
    distanceRadius = 100;
    venues: any[] = [];
    pageNumber: number = 1;
    showInfiniteScroll: boolean = true;
    @ViewChild(Refresher) refresher: Refresher;
    venueTypes: VenueType[] = [];
    showHours: boolean = false;
    date = new Date();
    dayOfWeek: number = 0;

    constructor(public userProvider: UserProvider, public loadingCtrl: LoadingController, public identityService: identityService, public events: Events, public navCtrl: NavController) {
        var radius = localStorage.getItem("DistanceRadius");
        if (radius != undefined && radius != null) {
            this.distanceRadius = parseInt(radius);
        }
        this.venueTypes = JSON.parse(localStorage.getItem("venueTypes"));
        this.loadSpots(this.pageNumber);
    }

    openSpot(venueId) {
        this.events.publish("openSpot", venueId);
    }

    doRefresh(refresher) {

        var spotDistance = new SpotDistance();
        spotDistance.Page = 1;
        spotDistance.Radius = this.distanceRadius;
        spotDistance.Latitude = +localStorage.getItem('Latitude');
        spotDistance.Longitude = +localStorage.getItem('Longitude');

        this.userProvider.loadSpotsCall(spotDistance).then((response: any) => {
            refresher.complete();
            if (response.WasSuccessful) {
                this.venues = response.Data;
                this.checkHours();
            } else {
                this.showErrorMessage(response)
            }
        })
            .catch(err => {
                refresher.complete();

                if (err.status == 401) {
                    if (localStorage.getItem("facebookId") != undefined && localStorage.getItem("facebookId") != null) {
                        var facebookLoginUserDetails = {
                            EmailAddress: localStorage.getItem("email"),
                            FacebookId: localStorage.getItem("FacebookExternalId"),
                        };
                        this.identityService.loginExternal(facebookLoginUserDetails, false);
                    } else {
                        var loginDetails = {
                            Username: localStorage.getItem("username"),
                            Password: localStorage.getItem("password"),
                        }
                        this.identityService.authenticateLogin(loginDetails, false);
                    }
                } else {
                    this.showErrorMessage(err)
                }

            });
    }

    ionViewDidEnter() {
        this.refresher.pullMax = 150;
        this.refresher.pullMin = 150;
    }

    loadSpots(page) {
        let loading = this.loadingCtrl.create({
            content: 'Loading Spots...'
        });

        debugger;
        loading.present();

        var spotDistance = new SpotDistance();
        spotDistance.Page = page;
        spotDistance.Radius = this.distanceRadius;
        spotDistance.Latitude = +localStorage.getItem('Latitude');
        spotDistance.Longitude = +localStorage.getItem('Longitude');

        this.userProvider.loadSpotsCall(spotDistance).then((response: any) => {
            loading.dismiss();
            if (response.WasSuccessful) {
                this.venues = response.Data;
                this.checkHours();
            } else {
                this.showErrorMessage(response)
            }
        })
            .catch(err => {
                loading.dismiss();

                if (err.status == 401) {
                    if (localStorage.getItem("facebookId") != undefined && localStorage.getItem("facebookId") != null) {
                        var facebookLoginUserDetails = {
                            EmailAddress: localStorage.getItem("email"),
                            FacebookId: localStorage.getItem("FacebookExternalId"),
                        };
                        this.identityService.loginExternal(facebookLoginUserDetails, false);
                    } else {
                        var loginDetails = {
                            Username: localStorage.getItem("username"),
                            Password: localStorage.getItem("password"),
                        }
                        this.identityService.authenticateLogin(loginDetails, false);
                    }
                } else {
                    this.showErrorMessage(err)
                }

            });
    }

    doInfinite(infiniteScroll) {

        var spotDistance = new SpotDistance();
        this.pageNumber += 1;
        spotDistance.Page = this.pageNumber;
        spotDistance.Radius = this.distanceRadius;
        spotDistance.Latitude = +localStorage.getItem('Latitude');
        spotDistance.Longitude = +localStorage.getItem('Longitude');

        this.userProvider.loadSpotsCall(spotDistance).then((response: any) => {
            infiniteScroll.complete();
            if (response.WasSuccessful) {
                if (response.Data.length < 5) {
                    this.showInfiniteScroll = false;
                }
                response.Data.forEach(element => {
                    this.venues.push(element);
                });
                this.checkHours();
            } else {
                if (response.Data.length < 0) {
                    this.showInfiniteScroll = false;
                } else {
                    this.showErrorMessage(response);
                }
                this.showInfiniteScroll = false;
            }
        })
            .catch(err => {
                infiniteScroll.complete();
                this.showErrorMessage(err)
            });
    }

    showErrorMessage(message) {
        if (message.Heading != undefined) {
            this.events.publish('ShowErrorAlert', message.Heading, message.Description);
        } else if (message._body != undefined && this.IsJsonString(message._body)) {
            message = JSON.parse(message._body);
            this.events.publish('ShowErrorAlert', message.Heading, message.Description);
        } else {
            console.error(message.toString());
            this.events.publish('ShowErrorAlert', "An error occured", "An error occured, please ensure you have a internet connection and try again");
        }
    }

    IsJsonString(str) {
        try {
            JSON.parse(str);
        } catch (e) {
            return false;
        }
        return true;
    }

    checkHours()
    {
        debugger;
        this.venues.forEach(i => {
            if(i.Hours != null){
                var hoursAvailable = i.Hours.Days == null ? false : true;
                var timesAdded = false;
                if(hoursAvailable)
                {
                    debugger;
                    i.Hours.Days.forEach(j => {
                        if(j.DayOfWeek == this.dayOfWeek)
                        {
                            i.Times = i.Hours.Days[this.dayOfWeek].Times;
                            timesAdded = true;
                        }
                    });
    
                    if(!timesAdded)
                    {
                        i.Times = null;
                    }
                }
            }
            else{
                i.Times = null;
            }   
        });
    }
}
