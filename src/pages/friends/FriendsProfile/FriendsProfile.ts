import { Component } from '@angular/core';
import { Events, NavParams, LoadingController } from 'ionic-angular';
import { UserProvider } from '../../../providers/userProvider';

@Component({
    selector: 'page-FriendsProfile',
    templateUrl: 'FriendsProfile.html'
})
export class FriendsProfile {
    name: string;
    gender: string;
    mobileNum: string;
    email: string;
    userId: any;

    constructor(public events: Events, public navParams: NavParams, public userProvider: UserProvider, public loadingCtrl: LoadingController) {
        this.userId = this.navParams.get("userId");
        this.loadProfile();
    }

    loadProfile() {
        let loading = this.loadingCtrl.create({
            content: 'Loading Profile...'
        });

        loading.present();

        this.userProvider.loadProfile(this.userId).then((response: any) => {

            if (response.WasSuccessful) {
                loading.dismiss();
                this.name = response.Data.FullName;
                this.mobileNum = response.Data.CellNumber;
                this.gender = response.Data.Gender;
                this.email = response.Data.EmailAddress;
            } else {
                loading.dismiss();
                this.showErrorMessage(response)
            }
        })
            .catch(err => {
                loading.dismiss();
                this.showErrorMessage(err)
            });
    }

    goBack() {
        this.events.publish('popNav');
    }

    showErrorMessage(message) {
        if (message.Heading != undefined) {
            this.events.publish('ShowErrorAlert', message.Heading, message.Description);
        } else if (message._body != undefined && this.IsJsonString(message._body)) {
            message = JSON.parse(message._body);
            this.events.publish('ShowErrorAlert', message.Heading, message.Description);
        } else {
            console.error(message.toString());
            this.events.publish('ShowErrorAlert', "An error occured", "An error occured, please ensure you have a internet connection and try again");
        }
    }

    IsJsonString(str) {
        try {
            JSON.parse(str);
        } catch (e) {
            return false;
        }
        return true;
    }

}
