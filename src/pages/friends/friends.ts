import { Component } from '@angular/core';
import { NavController, LoadingController, Events, AlertController } from 'ionic-angular';
import { UserProvider } from '../../providers/userProvider';
import { Keyboard } from '@ionic-native/keyboard';

@Component({
    selector: 'page-friends',
    templateUrl: 'friends.html'
})
export class FriendsPage {
    showFriends: boolean = false;
    selectedOption: string = 'friends';
    friends: any;
    friendRequests: any;
    term = "";
    friendsFound: any;
    noFriendsFound: boolean = false;

    constructor(public alertCtrl: AlertController, private keyboard: Keyboard, public events: Events, public userProvider: UserProvider, public loadingCtrl: LoadingController, public navCtrl: NavController) {
        this.loadFriends();
    }

    showFriendsMeth() {
        this.showFriends = !this.showFriends;
    }

    selectOption(option) {
        this.selectedOption = option;
    }

    goBack() {
            this.events.publish('popNav');
    }

    openFriend(userId){
        this.events.publish("openProfile", userId);
    }

    loadFriends() {
        let loading = this.loadingCtrl.create({
            content: 'Loading Friends...'
        });

        this.friends = null;
        this.friendRequests = null;

        loading.present();

        this.userProvider.getAllFriends().then((response: any) => {

            if (response.WasSuccessful) {
                this.friends = response.Data;
                this.loadFriendRequests(loading);
            } else {
                loading.dismiss();
                this.showErrorMessage(response)
            }
        })
            .catch(err => {
                loading.dismiss();
                this.showErrorMessage(err)
            });
    }

    loadFriendRequests(loading) {

        this.userProvider.getAllFriendRequests().then((response: any) => {
            loading.dismiss();
            if (response.WasSuccessful) {
                this.friendRequests = response.Data;
            } else {
                this.showErrorMessage(response)
            }
        })
            .catch(err => {
                loading.dismiss();
                this.showErrorMessage(err)
            });
    }

    addFriends(userId) {
        let loading = this.loadingCtrl.create({
            content: 'Sending Friend Request...'
        });

        loading.present();

        this.userProvider.addFriend(userId).then((response: any) => {
            loading.dismiss();
            if (response.WasSuccessful) {
                this.events.publish('showToast', "Your friend request has been successfully sent", true);
                this.friendsFound = null;
            } else {
                this.showErrorMessage(response)
            }
        })
            .catch(err => {
                loading.dismiss();
                this.showErrorMessage(err)
            });
    }

    declineFriend(userId, name) {
        let loading = this.loadingCtrl.create({
            content: 'Declining Friend Request...'
        });

        loading.present();

        this.userProvider.declineFriend(userId).then((response: any) => {
            loading.dismiss();
            if (response.WasSuccessful) {
                this.events.publish('showToast', "You have declined " + name + " friend request", false);
                this.loadFriends();
            } else {
                this.showErrorMessage(response)
            }
        })
            .catch(err => {
                loading.dismiss();
                this.showErrorMessage(err)
            });
    }

    confirmFriend(userId, name) {
        let loading = this.loadingCtrl.create({
            content: 'Confirming Friend Request...'
        });

        loading.present();

        this.userProvider.confirmFriend(userId).then((response: any) => {
            loading.dismiss();
            if (response.WasSuccessful) {
                this.events.publish('showToast', "You are now friends with " + name, true);
                this.loadFriends();
            } else {
                this.showErrorMessage(response)
            }
        })
            .catch(err => {
                loading.dismiss();
                this.showErrorMessage(err)
            });
    }

    clearSearch() {
        this.term = "";
        this.friendsFound = null;
    }

    cancelSearch() {
        this.term = "";
        this.friendsFound = null;
    }

    searchFriends(searchTerm) {

        this.userProvider.searchFreinds(searchTerm)
            .then((response: any) => {
                if (response.WasSuccessful) {
                    this.noFriendsFound = false;
                    this.friendsFound = response.Data;
                } else {
                    this.noFriendsFound = true;
                }

            })
            .catch(err => {
                //var errorResponse = JSON.parse(err._body);
                //this.showError(errorResponse);
                this.friendsFound = null;
                if (searchTerm == "") {
                    this.noFriendsFound = false;
                } else {
                    this.noFriendsFound = true;
                }

                console.log(err);
            });
    }

    addFriend(id, name) {
        const confirm = this.alertCtrl.create({
            title: 'Would you like to send ' + name + ' a friend request?',
            buttons: [
                {
                    text: 'Cancel'
                },
                {
                    text: 'Add Freind',
                    handler: () => {
                        this.addFriends(id);
                    }
                }
            ]
        });
        confirm.present();
    }

    acceptFriendRequest(id, name) {
        const confirm = this.alertCtrl.create({
            title: 'Would you like to accept ' + name + ' a friend request?',
            buttons: [
                {
                    text: 'Decline',
                    handler: () => {
                        this.declineFriend(id, name);
                    }
                },
                {
                    text: 'Accept',
                    handler: () => {
                        this.confirmFriend(id, name);
                    }
                }
            ]
        });
        confirm.present();
    }

    closeKeyboard() {
        this.keyboard.close();
    }

    showErrorMessage(message) {
        if (message.Heading != undefined) {
            this.events.publish('ShowErrorAlert', message.Heading, message.Description);
        } else if (message._body != undefined && this.IsJsonString(message._body)) {
            message = JSON.parse(message._body);
            this.events.publish('ShowErrorAlert', message.Heading, message.Description);
        } else {
            console.error(message.toString());
            this.events.publish('ShowErrorAlert', "An error occured", "An error occured, please ensure you have a internet connection and try again");
        }
    }

    IsJsonString(str) {
        try {
            JSON.parse(str);
        } catch (e) {
            return false;
        }
        return true;
    }

}

