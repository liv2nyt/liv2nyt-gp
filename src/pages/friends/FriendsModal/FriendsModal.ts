import { Component } from '@angular/core';
import { Events, NavParams, LoadingController, AlertController, ViewController } from 'ionic-angular';
import { UserProvider } from '../../../providers/userProvider';
import { AddFriend, InvitationDetails } from '../../../contracts/contracts';

@Component({
    selector: 'page-FriendsModal',
    templateUrl: 'FriendsModal.html'
})
export class FriendsModal {
    term = "";
    friendsFound: any;
    noFriendsFound: boolean = false;
    friends: any;
    friendsBackUp: any;
    venueId: number;

    constructor(private userProvider: UserProvider,
                private loadingCtrl: LoadingController,
                private events: Events,
                private alertCtrl: AlertController,
                private navParams: NavParams,
                private viewCtrl: ViewController)
    {
        this.venueId = this.navParams.get("venueId");
        this.loadFriends();
    }

    sendInvites()
    {
        debugger;
        let loading = this.loadingCtrl.create({
            content: 'Sending Invitations...'
        });

        loading.present();

        var friendsToInvite = this.friends.filter(x => x.Selected);

        if(friendsToInvite.length == 0)
        {
            this.showAlert('Please select atleast one friend to send invitation');
        }

        var friendsList: AddFriend[] = [];

        friendsToInvite.forEach(element => {
            var friend = new AddFriend()
            friend.UserId = element.UserId;

            friendsList.push(friend);
        });

        var inivitationRequest = new InvitationDetails();
        inivitationRequest.UserList = JSON.stringify(friendsList);
        inivitationRequest.VenueId = this.venueId;

        this.userProvider.SendInvitations(inivitationRequest).then((response: any) => {
            if (response.WasSuccessful) {
                loading.dismiss();
                this.dismiss(true);
            } else {
                loading.dismiss();
                this.showErrorMessage(response)
            }
        })
            .catch(err => {
                loading.dismiss();
                this.showErrorMessage(err)
            });

    }

    showAlert(message) {
        const prompt = this.alertCtrl.create({
            title: message,
            buttons: [
                {
                    text: 'Cancel'
                },
                {
                    text: 'Ok'
                }
            ]
        });
        prompt.present();
    }

    friendSelected(friend){
        debugger;
       var friend =  this.friends.filter(x => x.UserId === friend.UserId);
       
       if(friend[0].Selected == true)
       {
            friend[0].Selected = false;
       }
       else{
            friend[0].Selected = true;
       }
    }

    goBack() {
        //this.events.publish('popNav');
        this.dismiss(false);
    }

    loadFriends() {
        let loading = this.loadingCtrl.create({
            content: 'Loading Friends...'
        });

        this.friends = null;

        loading.present();

        this.userProvider.getAllFriendsWithInvites(this.venueId).then((response: any) => {

            if (response.WasSuccessful) {
                this.friends = response.Data;
                this.friendsBackUp = response.Data;
                loading.dismiss();
            } else {
                loading.dismiss();
                this.showErrorMessage(response)
            }
        })
            .catch(err => {
                loading.dismiss();
                this.showErrorMessage(err)
            });
    }

    searchFriends(searchTerm) {
        this.friends = this.friendsBackUp.filter(x => x.FullName.toLowerCase().indexOf(searchTerm.toLowerCase()) >= 0);
        /*
        this.userProvider.searchFreinds(searchTerm)
            .then((response: any) => {
                if (response.WasSuccessful) {
                    this.noFriendsFound = false;
                    this.friendsFound = response.Data;
                } else {
                    this.noFriendsFound = true;
                }

            })
            .catch(err => {
                //var errorResponse = JSON.parse(err._body);
                //this.showError(errorResponse);
                this.friendsFound = null;
                if (searchTerm == "") {
                    this.noFriendsFound = false;
                } else {
                    this.noFriendsFound = true;
                }

                console.log(err);
            });
            */
    }

    clearSearch() {
        this.term = "";
        this.friendsFound = null;
        this.friends = this.friendsBackUp;
    }

    cancelSearch() {
        this.term = "";
        this.friendsFound = null;
        this.friends = this.friendsBackUp;
    }

    showErrorMessage(message) {
        if (message.Heading != undefined) {
            this.events.publish('ShowErrorAlert', message.Heading, message.Description);
        } else if (message._body != undefined && this.IsJsonString(message._body)) {
            message = JSON.parse(message._body);
            this.events.publish('ShowErrorAlert', message.Heading, message.Description);
        } else {
            console.error(message.toString());
            this.events.publish('ShowErrorAlert', "An error occured", "An error occured, please ensure you have a internet connection and try again");
        }
    }

    IsJsonString(str) {
        try {
            JSON.parse(str);
        } catch (e) {
            return false;
        }
        return true;
    }

    dismiss(showSuccessAlert) {
        this.viewCtrl.dismiss(showSuccessAlert);
      }
}