import { Component } from '@angular/core';
import { NavController, AlertController, Events } from 'ionic-angular';
import { RegistrationPage } from '../registration/registration';
import { TabsPage } from '../tabs/tabs';
import { identityService } from '../../services/identityService'
import { LoginPage } from '../login/login';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook';


@Component({
        selector: 'page-landing',
        templateUrl: 'landing.html'
})
export class LandingPage {

        constructor(private fb: Facebook, public identityService: identityService, public navCtrl: NavController, public alertCtrl: AlertController, public events: Events) {

        }

        login() {
                this.navCtrl.push(LoginPage);
        }


        register() {
                this.navCtrl.push(RegistrationPage);
        }


        facebookLogin() {
                this.fb.login(['public_profile', 'user_friends', 'email'])
                        .then((res: FacebookLoginResponse) => {
                                console.log('Logged into Facebook!', res);
                                localStorage.setItem('FacebookAuthToken', res.authResponse.accessToken);
                                if (res.status == 'connected') {

                                        this.fb.api('/me?fields=id,name,email,picture.type(large),permissions', null).then((response: any) => {

                                                localStorage.setItem('profilePic', response.picture.data.url);
                                                localStorage.setItem('name', response.name);
                                                localStorage.setItem('facebookId', response.id);
                                                localStorage.setItem('email', response.email);

                                                var facebookLoginUserDetails = {
                                                        EmailAddress: response.email,
                                                        FacebookId: response.id,
                                                };

                                                this.identityService.loginExternal(facebookLoginUserDetails, false);


                                        })
                                                .catch(e => {
                                                        this.events.publish('ShowErrorAlert', 'Error retreiving information from facebook', "Please try again and ensure you have provided access to Liv2nyt");
                                                        console.log('Error retrieving user information from facebook', e.toString());
                                                });

                                }
                        })
                        .catch(e => {
                                this.events.publish('ShowErrorAlert', 'Error logging into Facebook', "Please ensure you have the facebook app installed on your device and try again");
                                console.log('Error logging into Facebook', e.toString());
                        });
        }

}
