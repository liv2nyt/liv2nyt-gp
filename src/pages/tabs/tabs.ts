import { Component } from '@angular/core';
import { LinkUpPage } from '../linkup/linkup';
import { SettingsPage } from '../settings/settings';
import { HomePage } from '../home/home';
@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = LinkUpPage;
  tab3Root = SettingsPage;

  constructor() {

  }
}
