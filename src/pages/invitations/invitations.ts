import { Component } from '@angular/core';
import { LoadingController, Events, AlertController } from 'ionic-angular';
import { UserProvider } from '../../providers/userProvider';

@Component({
    selector: 'page-invitations',
    templateUrl: 'invitations.html'
})
export class InvitationsPage {
    receivedInvites: any = [];
    sentInvites: any = [];
    noReceivedInvitesFound: boolean = false;
    noSentInvitesFound: boolean = false;
    selectedOption: string = 'received';

    constructor(
        public userProvider: UserProvider,
        public loadingCtrl: LoadingController,
        public events: Events,
        public alertCtrl: AlertController)
    {
        this.loadReceivedInvitations();
    }

    goBack() {
        this.events.publish('popNav');
    }

    openSpot(venueId) {
        this.events.publish("openSpot", venueId);
    }

    selectOption(option) {
        this.selectedOption = option;
    }

    loadReceivedInvitations(){
        debugger;
        let loading = this.loadingCtrl.create({
            content: 'Loading Invitations...'
        });

        loading.present();

        this.userProvider.getInvitationsReceived().then((response: any) => {
            debugger;
            if (response.WasSuccessful) {
                this.receivedInvites = response.Data;
                this.loadInvitationsSent(loading);
            } else {
                this.noReceivedInvitesFound = true;
                this.loadInvitationsSent(loading);
            }
        })
            .catch(err => {
                console.error(err);
                this.noReceivedInvitesFound = true;
                this.loadInvitationsSent(loading);
            });
    }

    loadInvitationsSent(loading){
        this.userProvider.getInvitationsSent().then((response: any) => {
            if (response.WasSuccessful) {
                this.sentInvites = response.Data;
                loading.dismiss();
            } else {
                loading.dismiss();
                this.noSentInvitesFound = true;
                this.showErrorMessage(response);
            }
        })
            .catch(err => {
                loading.dismiss();
                console.error(err);
                this.noSentInvitesFound = true;
            });
    }

    acceptInvitation(invitationId)
    {
        let loading = this.loadingCtrl.create({
            content: 'Accepting Invitation...'
        });

        loading.present();

        this.userProvider.acceptInvitation(invitationId).then((response: any) => {
            debugger;
            loading.dismiss();
            if (response.WasSuccessful) {
                this.presentAlert('Successully acccepted invitation');
                this.removeInvitation(invitationId);
            } else {
                this.showErrorMessage(response);
            }
        })
            .catch(err => {
                loading.dismiss();
                console.error(err);
            });
    }

    declineInvitation(invitationId)
    {
        let loading = this.loadingCtrl.create({
            content: 'Declining Invitation...'
        });

        loading.present();

        this.userProvider.declineInvitation(invitationId).then((response: any) => {
            loading.dismiss();
            if (response.WasSuccessful) {
                this.presentAlert('Successully declined invitation');
                this.removeInvitation(invitationId);
            } else {
                this.showErrorMessage(response);
            }
        })
            .catch(err => {
                loading.dismiss();
                console.error(err);
            });
    }

    removeInvitation(invitationId) {
        this.receivedInvites = this.receivedInvites.filter(x => x.InvitationId !== invitationId);
    }

    presentAlert(message)
    {
        const confirm = this.alertCtrl.create({
            title: message,
            buttons: [
                {
                    text: 'Ok',
                    handler: () => {

                    }
                }
            ]
        });
        confirm.present();
    }

    showErrorMessage(message) {
        if (message.Heading != undefined) {
            this.events.publish('ShowErrorAlert', message.Heading, message.Description);
        } else if (message._body != undefined && this.IsJsonString(message._body)) {
            message = JSON.parse(message._body);
            this.events.publish('ShowErrorAlert', message.Heading, message.Description);
        } else {
            console.error(message.toString());
            this.events.publish('ShowErrorAlert', "An error occured", "An error occured, please ensure you have a internet connection and try again");
        }
    }

    IsJsonString(str) {
        try {
            JSON.parse(str);
        } catch (e) {
            return false;
        }
        return true;
    }
}