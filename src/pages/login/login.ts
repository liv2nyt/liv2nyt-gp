import { Component } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { identityService } from '../../services/identityService';
import { Keyboard } from '@ionic-native/keyboard';

@Component({
        selector: 'page-login',
        templateUrl: 'login.html'
})
export class LoginPage {
        loginForm: FormGroup;
        submitAttempt: boolean = false;
        showPassword: boolean = false;

        constructor(private keyboard: Keyboard, public alertCtrl: AlertController, public identityService: identityService, public navCtrl: NavController, public formBuilder: FormBuilder) {
                this.loginForm = formBuilder.group({
                        username: ['', Validators.compose([Validators.minLength(2), Validators.required])],
                        password: ['', Validators.compose([Validators.minLength(6), Validators.required])],
                });
        }

        // Validators.compose([Validators.maxLength(30), Validators.pattern('[a-zA-Z ]*'), Validators.required])

        cancel() {
                this.navCtrl.pop();
        }

        closeKeyboard(){
                this.keyboard.close();
        }


        loginUser() {
                this.submitAttempt = true;

                if (!this.loginForm.controls.password.valid || !this.loginForm.controls.username.valid) {
                        const alert = this.alertCtrl.create({
                                title: 'Incomplete Form',
                                subTitle: 'Please complete all the fields correctly and then try again',
                                buttons: ['OK']
                        });
                        alert.present();
                } else {


                        var loginDetails = {
                                Username: this.loginForm.value.username,
                                Password: this.loginForm.value.password,
                        }

                        this.identityService.authenticateLogin(loginDetails, false);
                }


        }

        forgotPassword() {
                const alert = this.alertCtrl.create({
                        title: 'Still in construction',
                        subTitle: 'This feature is still in contruction',
                        buttons: ['OK']
                });
                alert.present();
        }

}
