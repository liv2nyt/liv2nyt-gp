import { Component } from '@angular/core';
import { NavController, AlertController, Events } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { TabsPage } from '../tabs/tabs';
import { identityService } from '../../services/identityService'
import { INTERNAL_BROWSER_PLATFORM_PROVIDERS } from '@angular/platform-browser/src/browser';
import { EmailValidator } from '../../services/validators/email';
import { ImageUploader } from '../../services/imageUploader';
import { Http, Headers } from '@angular/http';
import { Keyboard } from '@ionic-native/keyboard';

@Component({
        selector: 'page-registration',
        templateUrl: 'registration.html'
})
export class RegistrationPage {
        registrationForm: FormGroup;
        submitAttempt: boolean = false;
        step: string = '1';
        countryCode: string = "+27";
        showPassword: boolean = false;
        emailSubmitAttempt: boolean = false;
        mobileSubmitAttempt: boolean = false;
        imageUploadedSuccess: boolean = false;
        imageUrl: any = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTaiqeTVjJF7fRIakIA28Yuwo9jWPkbRbq6HAdwdopF3QwGvEIxGg";
        tinyImageUrl:any="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTaiqeTVjJF7fRIakIA28Yuwo9jWPkbRbq6HAdwdopF3QwGvEIxGg";
        constructor(private keyboard: Keyboard, public _http: Http, public events: Events, public imageUploader: ImageUploader, public alertCtrl: AlertController, public identityService: identityService, public navCtrl: NavController, public formBuilder: FormBuilder) {

                this.events.subscribe('RegisterComplete', () => {
                        //this.imageUrl = localStorage.getItem("profilePic");
                        this.step = '3';
                });

                this.events.subscribe('imageUploaded', () => {
                                this.imageUploadedSuccess = true;
                                this.imageUrl = localStorage.getItem("profilePic");
                                this.tinyImageUrl = localStorage.getItem("profilePicTiny");
                });
                

                let checkUsername = (control: FormControl): any => {
                        // in here, `db` is magically reliable
                        var headers = new Headers();
                        headers.append('Content-Type', 'application/json; charset=utf-8');

                        return new Promise((resolve, reject) => {
                                this._http.post('http://linkapi.liv2nyt.com/api/User/IsUsernameAvailalble/' + control.value.toLowerCase(), {
                                        headers: headers
                                }).subscribe(data => {
                                        resolve(null);
                                },
                                        error => {
                                                resolve({
                                                        "username taken": true
                                                });
                                        });
                        });
                }

                this.registrationForm = formBuilder.group({
                        username: ['', Validators.compose([Validators.minLength(2), Validators.required]), checkUsername],
                        FullName: ['', Validators.compose([Validators.required, Validators.pattern('[a-zA-Z ]*')])],
                        gender: ['Female'],
                        hashedPassword: ['', Validators.compose([Validators.minLength(6), Validators.required])],
                        emailAddress: ['', EmailValidator.isEmaillValid],
                        CellNumber: ['', Validators.compose([Validators.minLength(9), Validators.required])],
                        registrationType: ['mobileNumber'],
                        image: ['']
                });
        }


        closeKeyboard() {
                this.keyboard.close();
        }


        goToNextStep() {

                this.submitAttempt = true;
                if (this.step == '1') {
                        if (!this.registrationForm.controls.hashedPassword.valid || !this.registrationForm.controls.username.valid || !this.registrationForm.controls.FullName.valid) {
                                const alert = this.alertCtrl.create({
                                        title: 'Incomplete Form',
                                        subTitle: 'Please complete all the fields correctly and then try again',
                                        buttons: ['OK']
                                });
                                alert.present();
                        } else {
                                this.step = '2';
                        }

                } else if (this.step == '2') {
                        if (this.registrationForm.value.registrationType == "mobileNumber") {
                                this.mobileSubmitAttempt = true;
                                if (!this.registrationForm.controls.CellNumber.valid || this.countryCode == "") {
                                        const alert = this.alertCtrl.create({
                                                title: 'Invalid Mobile Number',
                                                subTitle: 'Please ensure you have correctly entered your mobile number and country code and try again',
                                                buttons: ['OK']
                                        });
                                        alert.present();
                                } else {
                                        if (this.countryCode.substring(0, 1) != '+') {
                                                var countryCodeStr = '+' + this.countryCode;
                                                this.countryCode = countryCodeStr;
                                        }

                                        if (this.registrationForm.value.CellNumber.substring(0, 1) == "0") {
                                                var mobileNumberStr = this.registrationForm.value.CellNumber.substring(1, this.registrationForm.value.CellNumber.length)
                                                this.registrationForm.value.CellNumber = mobileNumberStr;
                                        }

                                        //this.step = '3';
                                        this.registerUser();
                                }
                        } else {
                                if (!this.registrationForm.controls.emailAddress.valid) {
                                        this.emailSubmitAttempt = true;
                                        const alert = this.alertCtrl.create({
                                                title: 'Invalid Email Address',
                                                subTitle: 'Please ensure you have correctly entered your email address and try again',
                                                buttons: ['OK']
                                        });
                                        alert.present();
                                } else {
                                        //this.step = '3';
                                        this.registerUser();
                                }
                        }

                } else {
                        this.identityService.checkLocationSettings();
                }
        }

        uploadImage() {
                debugger;
                this.imageUploader.uploadImage("http://linkapi.liv2nyt.com/api/Image/User", "profile");

        }

        cancel() {

                if (this.step == '1') {
                        this.navCtrl.pop();
                } else if (this.step == '2') {
                        this.step = '1';
                }
        }

        skipStep() {

                if (this.step == '2') {
                        this.step = '3';
                } else if (this.step == '3') {
                        var loginDetails = {
                                Username: this.registrationForm.value.username,
                                Password: this.registrationForm.value.hashedPassword,
                        }

                        this.identityService.checkLocationSettings();

                }
        }

        registerUser() {
                if (this.registrationForm.value.registrationType != 'email') {
                        this.registrationForm.value.emailAddress = null;
                        if (this.registrationForm.value.CellNumber.substring(0, 1) != '+') {
                                this.registrationForm.value.CellNumber = this.countryCode + this.registrationForm.value.CellNumber;
                        }

                } else {
                        this.registrationForm.value.CellNumber = null;
                }

                this.identityService.registerUser(this.registrationForm.value);
        }

}
