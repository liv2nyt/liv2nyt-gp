import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { UserProvider } from '../../providers/userProvider';
import { Keyboard } from '@ionic-native/keyboard';
import { LoadingController, Events, NavController, AlertController } from 'ionic-angular';
import { ImageUploader } from '../../services/imageUploader';

@Component({
        selector: 'page-createVenue',
        templateUrl: 'createVenue.html'
})
export class CreateVenuePage {
        step: string = '1';
        createVenueForm: FormGroup;
        term = "";
        addressPredictions = [];
        addressSelected: boolean = true;
        address = "";
        venueId: any;
        noPicture: boolean = true;
        selectedCategories:any;
        selectedCategoriesString:any = [];
        selectedCategoriesStringPost:string = "";

        imageUrl: any = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTaiqeTVjJF7fRIakIA28Yuwo9jWPkbRbq6HAdwdopF3QwGvEIxGg";

        constructor(public alertCtrl: AlertController, public navCtrl: NavController, public imageUploader: ImageUploader, public events: Events, public keyboard: Keyboard, public loadingCtrl: LoadingController, public formBuilder: FormBuilder, public userProvider: UserProvider) {


                this.events.subscribe('venueImageUploaded', (url) => {
                        this.imageUrl = url;
                        this.noPicture = false;
                });

                this.createVenueForm = formBuilder.group({
                        Name: ['', Validators.compose([Validators.minLength(2), Validators.required])],
                        Description: ['', Validators.compose([Validators.required])],
                        About: [''],
                        Phone: ['', Validators.compose([Validators.minLength(9)])],
                        Website: [''],
                        Address: [''],
                        VenueTypes:[this.selectedCategoriesStringPost]
                });

        }

        selectCat() {
                let alert = this.alertCtrl.create();
                alert.setTitle('Which planets have you visited?');

                alert.addInput({
                        type: 'checkbox',
                        label: 'Night Club',
                        value: '1',
                        checked: false
                });
                alert.addInput({
                        type: 'checkbox',
                        label: 'Bar',
                        value: '2',
                        checked: false
                });
                alert.addInput({
                        type: 'checkbox',
                        label: 'Lounge',
                        value: '3',
                        checked: false
                });
                alert.addInput({
                        type: 'checkbox',
                        label: 'Pub',
                        value: '4',
                        checked: false
                });
                alert.addInput({
                        type: 'checkbox',
                        label: 'Restaurant',
                        value: '5',
                        checked: false
                });
                alert.addInput({
                        type: 'checkbox',
                        label: 'Cocktail Bar',
                        value: '6',
                        checked: false
                });
                alert.addInput({
                        type: 'checkbox',
                        label: 'Rooftop',
                        value: '7',
                        checked: false
                });

                alert.addButton('Cancel');
                alert.addButton({
                        text: 'Okay',
                        handler: data => {
                                console.log('Checkbox data:', data);
                                this.selectedCategories = data;
                                if(this.selectedCategories.length > 0){
                                        for (let index = 0; index < this.selectedCategories.length; index++) {
                                                this.selectedCategoriesString.push(+this.selectedCategories[index]);
                                        }
                                        this.selectedCategoriesString = JSON.stringify(this.selectedCategoriesString);
                                        this.selectedCategoriesStringPost = this.selectedCategoriesString;
                                }
                                debugger;
                        }
                });
                alert.present();

        }

        goHome() {
                this.navCtrl.pop();
        }

        getPredictions(searchTerm) {
                this.userProvider.getAddressPredictions(searchTerm)
                        .then((response: any) => {
                                this.addressPredictions = response.Data;
                        })
                        .catch(err => {
                                console.log(err);
                        });
        }

        cancelSearch() {
                this.addressSelected = false;
                this.address = "";
                this.term = "";
                this.addressPredictions = null;
        }

        clearSearch() {
                this.addressSelected = false;
                this.address = "";
                this.term = "";
                this.addressPredictions = null;
        }

        chooseAddress(address) {
                debugger;
                this.address = address;
                this.createVenueForm.value.Address = address;
                this.addressSelected = true;
                this.term = address;
                this.addressPredictions = [];
        }

        createVenue() {
                if (this.createVenueForm.value.Address != "" && this.createVenueForm.value.Address != null && this.createVenueForm.value.Address != undefined) {
                        let loading = this.loadingCtrl.create({
                                content: 'Creating Venue...'
                        });

                        loading.present();
                        this.createVenueForm.value.Address = this.address;
                        this.createVenueForm.value.AboutUs = this.createVenueForm.value.Description;
                        debugger;
                        this.userProvider.createVenue(this.createVenueForm.value).then((response: any) => {
                                loading.dismiss();
                                if (response.WasSuccessful) {
                                        console.log("Event Created Success");
                                        this.events.publish("showToast", "Your venue has been created successfully", true);
                                        this.venueId = response.Data.Id;
                                        this.step = '2';
                                } else {
                                        this.showErrorMessage(response)
                                }
                        })
                                .catch(err => {
                                        loading.dismiss();
                                        this.showErrorMessage(err)
                                });
                } else {
                        const confirm = this.alertCtrl.create({
                                title: 'Please ensure you have entered a correct Address',
                                buttons: [
                                        {
                                                text: 'Ok'
                                        },
                                ]
                        });
                        confirm.present();
                }

        }

        closeKeyboard() {
                this.keyboard.close();
        }

        showErrorMessage(message) {
                if (message.Heading != undefined) {
                        this.events.publish('ShowErrorAlert', message.Heading, message.Description);
                } else if (message._body != undefined && this.IsJsonString(message._body)) {
                        message = JSON.parse(message._body);
                        this.events.publish('ShowErrorAlert', message.Heading, message.Description);
                } else {
                        console.error(message.toString());
                        this.events.publish('ShowErrorAlert', "An error occured", "An error occured, please ensure you have a internet connection and try again");
                }
        }

        IsJsonString(str) {
                try {
                        JSON.parse(str);
                } catch (e) {
                        return false;
                }
                return true;
        }

        uploadImage() {
                this.imageUploader.uploadImage("http://linkapi.liv2nyt.com/api/Image/Venue/" + this.venueId, "venue");
        }


}
