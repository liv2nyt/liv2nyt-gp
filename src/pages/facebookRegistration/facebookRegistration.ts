import { Component } from '@angular/core';
import { NavController, AlertController, Events } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { TabsPage } from '../tabs/tabs';
import { identityService } from '../../services/identityService'
import { ImageUploader } from '../../services/imageUploader';
import { Http, Headers } from '@angular/http';
import { Keyboard } from '@ionic-native/keyboard';

@Component({
        selector: 'page-facebookRegistration',
        templateUrl: 'facebookRegistration.html'
})
export class FacebookRegistrationPage {
        registrationForm: FormGroup;
        submitAttempt: boolean = false;
        step: string = '1';
        countryCode: string = "+27";
        mobileSubmitAttempt: boolean = false;
        imageUploadedSuccess:boolean = false;
        imageUrl:any = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTaiqeTVjJF7fRIakIA28Yuwo9jWPkbRbq6HAdwdopF3QwGvEIxGg";

        constructor(private keyboard: Keyboard, public _http:Http, public events: Events, public imageUploader: ImageUploader,public alertCtrl: AlertController, public identityService: identityService, public navCtrl: NavController, public formBuilder: FormBuilder) {
                
                this.events.subscribe('RegisterCompleteExternal', () => {
                        this.step = '3';
                });
                
                let checkUsername = (control: FormControl): any => {
                        // in here, `db` is magically reliable
                        var headers = new Headers();
                        headers.append('Content-Type', 'application/json; charset=utf-8');
                  
                        return new Promise((resolve, reject) => {
                          this._http.post('http://linkapi.liv2nyt.com/api/User/IsUsernameAvailalble/' + control.value.toLowerCase(), {
                            headers: headers
                          }) .subscribe(data => {
                              resolve(null);
                            },
                              error => {
                                resolve({
                                  "username taken": true
                                });
                              });
                        });
                }
                
                this.registrationForm = formBuilder.group({
                        Username: ['', Validators.compose([Validators.minLength(2), Validators.required]), checkUsername],
                        Gender: ['Female'],
                        CellNumber: ['', Validators.compose([Validators.minLength(9)])],
                        Image: [null],
                        EmailAddress: [localStorage.getItem("email")],
                        FacebookId: [localStorage.getItem("facebookId")],
                        FacebookAuthToken: [localStorage.getItem("FacebookAuthToken")],
                        FullName: [localStorage.getItem("name")],
                });
        }

        closeKeyboard(){
                this.keyboard.close();
        }

        goToNextStep() {

                this.submitAttempt = true;
                if (this.step == '1') {
                        if (!this.registrationForm.controls.Username.valid) {
                                const alert = this.alertCtrl.create({
                                        title: 'Incomplete Form',
                                        subTitle: 'Please complete all the fields correctly and then try again',
                                        buttons: ['OK']
                                });
                                alert.present();
                        } else {
                                this.step = '2';
                        }

                } else if (this.step == '2') {
                        this.mobileSubmitAttempt = true;
                        if (!this.registrationForm.controls.CellNumber.valid || this.countryCode == "" || this.registrationForm.value.CellNumber == "") {
                                const alert = this.alertCtrl.create({
                                        title: 'Invalid Mobile Number',
                                        subTitle: 'Please ensure you have correctly entered your mobile number and try again',
                                        buttons: ['OK']
                                });
                                alert.present();
                        } else {
                                if (this.countryCode != null && this.countryCode.substring(0, 1) != '+') {
                                        var countryCodeStr = ""
                                        countryCodeStr = '+' + this.countryCode;
                                        this.countryCode = countryCodeStr;
                                }

                                if (this.registrationForm.value.CellNumber != null &&  this.registrationForm.value.CellNumber.substring(0, 1) == "0") {
                                        var mobileNumberStr = "";
                                        mobileNumberStr = this.registrationForm.value.CellNumber.substring(1, this.registrationForm.value.CellNumber.length)
                                        this.registrationForm.value.CellNumber = mobileNumberStr;
                                }

                                this.registerUser();
                        }


                } else {
                        this.identityService.checkLocationSettings();
                }
        }

        uploadImage(){

                this.imageUploader.uploadImage("http://linkapi.liv2nyt.com/api/Image/User","profile").then((response) => {

                        if(response != null && response != undefined){
                                this.imageUploadedSuccess = true;
                                this.imageUrl = response;
                        }else{
                                this.events.publish('ShowErrorAlert', 'Failed to Upload Image', "The image failed to upload, please ensure you have an internet conenction and try again");
                        }
                        
                })
                .catch(err => {
                        //this.events.publish('ShowErrorAlert', 'Failed to Upload Image', "An error occured opening your gallery");
                        console.error(err.toString());
                });

        }

        cancel() {

                if (this.step == '1') {
                        this.navCtrl.pop();
                } else if (this.step == '2') {
                        this.step = '1';
                } 
        }

        skipStep() {

                if (this.step == '2') {
                        this.registrationForm.value.CellNumber = null;
                        this.registerUser();
                } else if (this.step == '3') {
                        this.identityService.checkLocationSettings();
                }
        }

        registerUser() {
        if(this.registrationForm.value.CellNumber != null && this.registrationForm.value.CellNumber.substring(0,1) != '+')
        {
                this.registrationForm.value.CellNumber = this.countryCode + this.registrationForm.value.CellNumber;
        }else{
                this.registrationForm.value.CellNumber = null;
        }
                
                this.identityService.registerExternalUser(this.registrationForm.value);
        }

}
