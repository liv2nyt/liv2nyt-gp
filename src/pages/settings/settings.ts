import { Component } from '@angular/core';
import { NavController, ModalController, Events, AlertController, Alert } from 'ionic-angular';
import { LandingPage } from '../landing/landing';
import { App } from 'ionic-angular';
import { LocationModal } from '../locationModal/locationModal';
import { identityService } from '../../services/identityService';
import { ImageUploader } from '../../services/imageUploader';

@Component({
    selector: 'page-settings',
    templateUrl: 'settings.html'
})
export class SettingsPage {
    name: string;
    gender: string;
    mobileNum: string;
    email: string;
    imageUrl: string;
    tinyImageUrl: string;

    constructor(public imageUploader:ImageUploader,public identityService: identityService, public alertCtrl: AlertController, public events: Events, public navCtrl: NavController, public appCtrl: App, public modalCtrl: ModalController) {
        this.events.subscribe('updateProfile', () => {
            this.name = localStorage.getItem('name');
            this.gender = localStorage.getItem('gender');
            this.mobileNum = localStorage.getItem('mobileNumber');
            this.email = localStorage.getItem('email');
        });

        this.events.subscribe('imageUploaded', () => {
            this.imageUrl = localStorage.getItem("profilePic");
            this.tinyImageUrl = localStorage.getItem("profilePicTiny");
        });

        this.name = localStorage.getItem('name');
        this.gender = localStorage.getItem('gender');
        this.mobileNum = localStorage.getItem('mobileNumber');
        this.email = localStorage.getItem('email');

        this.imageUrl = localStorage.getItem('profilePic');
        this.tinyImageUrl = localStorage.getItem("profilePicTiny");

        if (this.imageUrl == null || this.imageUrl == undefined || this.imageUrl == "") {
            this.imageUrl = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTaiqeTVjJF7fRIakIA28Yuwo9jWPkbRbq6HAdwdopF3QwGvEIxGg";
            this.tinyImageUrl = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTaiqeTVjJF7fRIakIA28Yuwo9jWPkbRbq6HAdwdopF3QwGvEIxGg";
        }
    }

    goToSettings() {
        this.navCtrl.push(LocationModal);
    }

    presentLocationModal() {
        this.events.publish("openLocationSettings");
    }

    openCreateVenue() {
        this.events.publish("openCreateVenue");
    }

    logout() {
        localStorage.clear();
        this.appCtrl.getRootNav().setRoot(LandingPage);
    }

    showFullNamePrompt() {
        const prompt = this.alertCtrl.create({
            title: 'Name',
            inputs: [
                {
                    value: this.name,
                    name: "FullName",
                },
            ],
            buttons: [
                {
                    text: 'Cancel'
                },
                {
                    text: 'Update',
                    handler: data => {
                        if (data.FullName.length < 1) {
                            const alert = this.alertCtrl.create({
                                title: 'Invalid Name',
                                subTitle: 'Please ensure you have correctly entered your full name and try again',
                                buttons: ['OK']
                            });
                            alert.present();
                        } else {
                            this.updateUser(data);
                        }
                    }
                }
            ]
        });
        prompt.present();
    }

    showConfirmExit() {
        const confirm = this.alertCtrl.create({
            title: 'Are you sure you want to Logout?',
            buttons: [
                {
                    text: 'Cancel'
                },
                {
                    text: 'Logout',
                    handler: () => {
                        this.logout();
                    }
                }
            ]
        });
        confirm.present();
    }

    showEmailPrompt() {
        const prompt = this.alertCtrl.create({
            title: 'Email',
            inputs: [
                {
                    placeholder: this.email == 'null' ? "Email Address" : "",
                    value: this.email == 'null' ? "" : this.email,
                    name: "EmailAddress",
                },
            ],
            buttons: [
                {
                    text: 'Cancel'
                },
                {
                    text: 'Update',
                    handler: data => {
                        if (data.EmailAddress.length < 3 || data.EmailAddress.indexOf("@") == -1) {
                            const alert = this.alertCtrl.create({
                                title: 'Invalid Email Address',
                                subTitle: 'Please ensure you have correctly entered your email address and try again',
                                buttons: ['OK']
                            });
                            alert.present();
                        } else {
                            this.updateUser(data);
                        }

                    }
                }
            ]
        });
        prompt.present();
    }

    showMobileNumPrompt() {
        const prompt = this.alertCtrl.create({

            title: 'Mobile Number',
            inputs: [
                {
                    placeholder: this.mobileNum == 'null' ? "Country Code (+27)" : "Country Code (+27)",
                    value: this.mobileNum == 'null' ? "" : this.mobileNum.substr(0, 3),
                    name: "CountryCode",
                },
                {
                    placeholder: this.mobileNum == 'null' ? "Mobile Number (827418525)" : "Mobile Number (827418525)",
                    value: this.mobileNum == 'null' ? "" : this.mobileNum.substr(3, this.mobileNum.length),
                    name: "CellNumber",
                },
            ],
            buttons: [
                {
                    text: 'Cancel'
                },
                {
                    text: 'Update',
                    handler: data => {
                        if (data.CellNumber.length < 9 || data.CountryCode.length < 3) {
                            const alert = this.alertCtrl.create({
                                title: 'Invalid Mobile Number',
                                subTitle: 'Please ensure you have correctly entered your mobile number and country code and try again',
                                buttons: ['OK']
                            });
                            alert.present();
                        } else {
                            if (data.CountryCode.substring(0, 1) != '+') {
                                var countryCodeStr = '+' + data.CountryCode;
                                data.CountryCode = countryCodeStr;
                            }

                            if (data.CellNumber.substring(0, 1) == "0") {
                                var mobileNumberStr = data.CellNumber.substring(1, data.CellNumber.length)
                                data.CellNumber = mobileNumberStr;
                            }

                            data.CellNumber = data.CountryCode + data.CellNumber;
                            this.updateUser(data);
                        }

                    }
                }
            ]
        });
        prompt.present();
    }

    openFriends() {
        this.events.publish("RouteBroadcastPush", "FriendsPage");
    }

    uploadImage() {
        this.imageUploader.uploadImage("http://linkapi.liv2nyt.com/api/Image/User", "profile");

    }

    showGenderRadio() {
        let alert = this.alertCtrl.create();
        alert.setTitle('Gender');

        if (this.gender == "Male") {

            alert.addInput({
                type: 'radio',
                label: 'Male',
                value: 'Male',
                checked: true
            });

            alert.addInput({
                type: 'radio',
                label: 'Female',
                value: 'Female',
                checked: false
            });

        } else {

            alert.addInput({
                type: 'radio',
                label: 'Male',
                value: 'Male',
                checked: false
            });

            alert.addInput({
                type: 'radio',
                label: 'Female',
                value: 'Female',
                checked: true
            });

        }

        alert.addButton('Cancel');
        alert.addButton({
            text: 'Update',
            handler: data => {
                var gender = {
                    "Gender": data
                }

                this.updateUser(gender);
            }
        });
        alert.present();
    }

    updateUser(profileDetails) {
        this.identityService.updateProfile(profileDetails);
    }

    openInvitations() {
        this.events.publish("invitations");
    }
}
