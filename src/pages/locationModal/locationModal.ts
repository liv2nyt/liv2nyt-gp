import { Component } from '@angular/core';
import { UserProvider } from '../../providers/userProvider';
import { AlertController, LoadingController, Events, NavParams } from 'ionic-angular';
import { Diagnostic } from '@ionic-native/diagnostic';
import { locationService } from '../../Services/locationService';
import { Keyboard } from '@ionic-native/keyboard';
import {
    GoogleMaps,
    GoogleMap,
    GoogleMapsEvent,
    GoogleMapOptions,
    CameraPosition,
    MarkerOptions,
    Marker,
    Environment
} from '@ionic-native/google-maps';
import { Platform } from 'ionic-angular/platform/platform';

@Component({
    templateUrl: 'locationModal.html',
    selector: 'page-locationModal',
})
export class LocationModal {
    environment: Environment = null;
    term = "";
    address = "";
    showButtons: boolean = false;
    addressPredictions = [];
    addressSaved: string = localStorage.getItem("Address");
    addressSelected: boolean = true;
    showEdit = true;
    map: GoogleMap;
    appConfigured = localStorage.getItem("AppConfigured");
    locationPreference = localStorage.getItem("LocationPreference");
    lat: any = localStorage.getItem("Latitude");
    long: any = localStorage.getItem("Longitude");
    useGps: any = this.locationPreference == "GPS" ? true : false;
    useAddress: any = this.locationPreference == "Address" ? true : false;
    radiusChanged: boolean = false;
    hardBackPress: boolean = false;
    distanceRadius: number = 100;
    userAddressBook: any[] = [];
    showMap: boolean = false;
    initOpen: boolean = false;

    constructor(public userProvider: UserProvider,
        public alertCtrl: AlertController,
        public events: Events,
        private keyboard: Keyboard,
        public locationService: locationService,
        public diagnostic: Diagnostic,
        public loadingCtrl: LoadingController,
        platform: Platform) {

        platform.registerBackButtonAction(() => {
            if (this.addressSaved == null && !this.useGps) {
                var alert = this.alertCtrl.create({
                    title: "No Location Saved",
                    subTitle: "Please add your location to continue",
                    buttons: ['OK']
                });
                alert.present();
            }
            else {
                this.events.publish('popNav');
            }
        }, 1);

        if (this.addressSaved == undefined) {
            this.initOpen = true;
        }

        if (localStorage.getItem("DistanceRadius") != null) {
            this.distanceRadius = +localStorage.getItem("DistanceRadius");
        };

        if (localStorage.getItem("AddressBook") != undefined && localStorage.getItem("AddressBook") != "") {
            this.userAddressBook = JSON.parse(localStorage.getItem("AddressBook"));
        }
        debugger;

        var t = localStorage.getItem("venueTypes");

        if (localStorage.getItem("venueTypes") == undefined && localStorage.getItem("venueTypes") == null) {
            var temp = this.userProvider.initializeVenueTypes();
        }

        //platform.registerBackButtonAction(() => this.dismiss());
    }

    ionViewDidLoad() {
        this.lat = localStorage.getItem("Latitude");
        this.long = localStorage.getItem("Longitude");
        if (this.lat != undefined && this.long != undefined) {
            this.showMap = true;
            this.loadMap();
        } else {
            this.showMap = false;
        }

    }

    loadMap() {

        var isApp = (!document.URL.startsWith('http') || document.URL.startsWith('http://localhost:8080'));

        if (isApp) {
            this.environment = new Environment();
            this.environment.setBackgroundColor("#0D1720");
            let mapOptions: GoogleMapOptions = {
                camera: {
                    target: {
                        lat: +this.lat,
                        lng: +this.long
                    },
                    zoom: 18,
                    tilt: 30,

                }
            };

            this.map = GoogleMaps.create('map', mapOptions);

            let marker: Marker = this.map.addMarkerSync({
                title: localStorage.getItem('name'),
                icon: 'blue',
                animation: 'DROP',
                position: {
                    lat: +this.lat,
                    lng: +this.long,
                }
            });
            /*marker.on(GoogleMapsEvent.MARKER_CLICK).subscribe(() => {
                alert('clicked');
            });*/
        } else {
            this.showMap = false;
        }

    }

    updateRadius() {
        this.radiusChanged = true;

    }

    goBack() {
        if (this.addressSaved == null && !this.useGps) {
            var alert = this.alertCtrl.create({
                title: "No Location Saved",
                subTitle: "Please add your location to continue",
                buttons: ['OK']
            });
            alert.present();
        }
        else {
            this.events.publish('popNav');
        }
    }

    saveRadius() {
        if ((this.addressSaved != null && this.addressSaved != undefined && this.addressSaved != "") || this.useGps) {
            this.radiusChanged = false;
            localStorage.setItem("DistanceRadius", this.distanceRadius.toString());
            this.events.publish('RouteBroadcastRoot', 'TabsPage');
        } else {
            var alert = this.alertCtrl.create({
                title: "No Location Saved",
                subTitle: "Please add your location to continue",
                buttons: ['OK']
            });
            alert.present();
        }

    }

    /*ionViewDidLoad() {
        this.navBar.backButtonClick = (e: UIEvent) => {
            if (this.addressSaved == null && !this.useGps) {
                var alert = this.alertCtrl.create({
                    title: "No Location Saved",
                    subTitle: "Please add your location to continue",
                    buttons: ['OK']
                });
                alert.present();
            }
            else {
                this.events.publish('popNav');
            }
        }
    }*/

    dismiss() {
        if (this.addressSaved == null && !this.useGps) {
            var alert = this.alertCtrl.create({
                title: "No Location Saved",
                subTitle: "Please add your location to continue",
                buttons: ['OK']
            });
            alert.present();
        }
        else {
            localStorage.setItem("DistanceRadius", this.distanceRadius.toString());
            this.events.publish('RouteBroadcastRoot', 'TabsPage');
        }

    }

    editAddress() {
        this.radiusChanged = false;
        this.addressSelected = false;
        this.showEdit = false;
    }

    cancelSearch() {
        this.addressSelected = false;
        this.address = "";
        this.term = "";
        this.addressPredictions = null;
    }

    clearSearch() {
        localStorage.setItem("DistanceRadius", this.distanceRadius.toString());
        this.addressSelected = false;
        this.address = "";
        this.term = "";
        this.addressPredictions = null;
        this.showButtons = false;
    }

    showError(errorResponse) {
        var alert = this.alertCtrl.create({
            title: errorResponse.Heading,
            subTitle: errorResponse.Description,
            buttons: ['OK']
        });
        alert.present();
    }

    selectedAddress(address) {
        this.address = address;
        this.confirmAddress();
    }

    clearAddressHistory() {
        this.userAddressBook = [];
        localStorage.setItem("AddressBook", "");
    }

    saveAddress() {
        if (localStorage.getItem("AddressBook") != undefined && localStorage.getItem("AddressBook") != "") {
            var addressbook = JSON.parse(localStorage.getItem("AddressBook"));
            addressbook.push(this.address);
            addressbook = JSON.stringify(addressbook);
            localStorage.setItem("AddressBook", addressbook);
        } else {
            var addressbook: any = [];
            addressbook.push(this.address);
            addressbook = JSON.stringify(addressbook);
            localStorage.setItem("AddressBook", addressbook);
        }
        this.confirmAddress();
    }

    confirmAddress() {
        var loading = this.loadingCtrl.create({
            content: 'Retrieving Location....'
        });

        loading.present();
        this.userProvider.getLatLong(this.address)
            .then((response: any) => {
                localStorage.setItem('Latitude', response.Data.Latitude);
                localStorage.setItem('Longitude', response.Data.Longitude);
                localStorage.setItem("LocationPreference", "Address");
                localStorage.setItem("AppConfigured", "True");
                localStorage.setItem("Address", this.address);

                this.addressSaved = this.address;
                this.address = "";
                this.addressSelected = true;
                this.showButtons = false;
                this.term = "";
                this.addressPredictions = null;
                this.showEdit = true;
                //this.dismiss();
                loading.dismiss();
                localStorage.setItem("DistanceRadius", this.distanceRadius.toString());
                this.events.publish('showToast', "Viewing events from: " + this.addressSaved, true);
                this.events.publish('RouteBroadcastRoot', 'TabsPage');
            })
            .catch(err => {
                loading.dismiss();
                //var errorResponse = JSON.parse(err._body);
                this.showError(err);
                console.log(err);
            });
    }

    getPredictions(searchTerm) {
        this.radiusChanged = false;
        this.userProvider.getAddressPredictions(searchTerm)
            .then((response: any) => {
                this.addressPredictions = response.Data;
            })
            .catch(err => {
                //var errorResponse = JSON.parse(err._body);
                //this.showError(errorResponse);
                console.log(err);
            });
    }

    chooseAddress(address) {
        this.address = address;
        this.addressSelected = true;
        this.term = address;
        this.showButtons = true;
        this.radiusChanged = false;
    }

    gpsToggle() {
        if (this.useGps) {
            this.useAddress = false;
            this.useGps = true;
            this.gpsFirstRun();
        }
        else {
            this.useAddress = true;
            this.useGps = false;
        }
    }

    addressToggle() {
        if (this.useAddress) {
            this.useGps = false;
            this.useAddress = true;
        }
        else {
            this.useGps = true;
            this.useAddress = false;
        }
    }

    clearAddress() {
        this.address = "";
        this.addressSelected = false;
        this.term = "";
        this.addressPredictions = null;
    }

    closeKeyboard() {
        this.keyboard.close();
    }

    gpsFirstRun() {
        this.locationService.isLocationAuthorized().subscribe(response => {
            if (response) {
                console.log("Location Authorized");
                this.locationService.getGpsAvailability().subscribe(response => {
                    if (response) {
                        console.log("GPS Available");
                        this.locationService.getUserLocation().subscribe(response => {
                            console.log("Location Retrieved");
                            localStorage.setItem("DistanceRadius", this.distanceRadius.toString());
                            this.events.publish('showToast', "We have successfully located your device", true);
                            this.events.publish('RouteBroadcastRoot', 'TabsPage');
                        },
                            error => {
                                console.log("Location Not Retrieved");
                                this.showGpsErrorAlert();
                            });
                    }
                    else {
                        console.log("GPS Not Available");
                        this.showEnableLocationAlert();
                    }
                },
                    error => {
                        console.log("GPS Not Available Error");
                        this.showEnableLocationAlert();
                    });
            }
            else {
                console.log("Location Not Authorized");
                this.showGpsUnauthorizedAlert();
            }

        },
            error => {
                this.locationService.isDeviceLocationEnabled().subscribe(response => {
                    console.log("Device Location Enabled");
                    this.gpsFirstRun();

                },
                    error => {
                        console.log("Device Location Not Enabled Error");
                        this.showEnableLocationAlert();
                    });
            });
    }

    showGpsUnavailableAlert() {
        var alert = this.alertCtrl.create({
            title: "GPS Not Available",
            subTitle: "Please Try Again",
            buttons: ['OK']
        });
        localStorage.setItem("LocationPreference", "");
        this.useAddress = false;
        this.useGps = false;

        alert.present();
    }

    showGpsUnauthorizedAlert() {
        var alert = this.alertCtrl.create({
            title: "GPS Permission Not Granted",
            subTitle: "Please Try Again",
            buttons: ['OK']
        });
        localStorage.setItem("LocationPreference", "");
        this.useAddress = false;
        this.useGps = false;

        alert.present();
    }

    showGpsErrorAlert() {
        var alert = this.alertCtrl.create({
            title: "Unable to find gps coordinates",
            subTitle: "Please Try Again",
            buttons: ['OK']
        });
        localStorage.setItem("LocationPreference", "");
        this.useGps = false;

        alert.present();
    }

    showEnableLocationAlert() {
        var alert = this.alertCtrl.create({
            title: "GPS not enabled",
            subTitle: "Please enable your device's GPS.",
            buttons: [{
                text: 'Location Settings',
                role: 'cancel',
                handler: () => {
                    this.diagnostic.switchToLocationSettings();
                    setTimeout(() => {
                        this.gpsFirstRun();
                    }, 2000);
                }
            },
            {
                text: 'Cancel',
                role: 'cancel',
                handler: () => {
                    localStorage.setItem("LocationPreference", "");
                    this.useGps = false;
                }
            }]
        });
        alert.present();
    }

}