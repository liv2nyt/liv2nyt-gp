import { Component } from '@angular/core';
import { NavController, Events, LoadingController, NavParams, AlertController, ModalController } from 'ionic-angular';
import { UserProvider } from '../../providers/userProvider';
import { LaunchNavigator, LaunchNavigatorOptions } from '@ionic-native/launch-navigator';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { SocialSharing } from '@ionic-native/social-sharing';
import { VenueType, Hours } from '../../contracts/contracts';
import { FriendsModal } from '../friends/FriendsModal/FriendsModal';

@Component({
    selector: 'page-spot',
    templateUrl: 'spot.html'
})
export class SpotPage {
    spotImages: any = [];
    openShare: boolean = false;
    venueId: number = 0;
    venueData: any;
    start: string;
    destination: string;
    checkedIn: boolean = false;
    venueTypes: VenueType[] = [];
    hours: Hours = new Hours;
    weekday = [];
    hoursAvailable: boolean = false;
    showHours: boolean = false;

    constructor(private socialShareing: SocialSharing, private iab: InAppBrowser, public launchNav: LaunchNavigator, public navParams: NavParams, public userProvider: UserProvider, public loadingCtrl: LoadingController, public events: Events, public navCtrl: NavController, public alertCtrl: AlertController, private modalCtrl: ModalController) {
        this.venueTypes = this.userProvider.initializeVenueTypes();
        this.setWeekDay();
        this.loadSpot(this.navParams.get("venueId"));
    }

    goBack() {
        this.events.publish('popNav');
    }

    shareClick() {
        if (this.openShare == false) {
            this.openShare = true;
        } else {
            this.openShare = false;
        }
    }

    loadSpot(venueId) {
        let loading = this.loadingCtrl.create({
            content: 'Loading Spot...'
        });

        loading.present();

        var spotDistance = {
            VenueId: venueId,
            Latitude: 0,
            Longitude: 0
        };
        spotDistance.Latitude = +localStorage.getItem('Latitude');
        spotDistance.Longitude = +localStorage.getItem('Longitude');

        this.userProvider.loadSpotCall(spotDistance).then((response: any) => {
            loading.dismiss();
            if (response.WasSuccessful) {
                this.venueData = response.Data;
                this.checkedIn = this.venueData.CheckedIn;
                this.hours = response.Data.Hours;
                debugger;
                if(this.hours != null){
                    this.hoursAvailable = response.Data.Hours.Days == null ? false : true;
                }else{
                    this.hoursAvailable = false;
                }
                //this.venues = response.Data;
            } else {
                this.showErrorMessage(response)
            }
        })
            .catch(err => {
                loading.dismiss();
                this.showErrorMessage(err)
            });
    }

    showErrorMessage(message) {
        if (message.Heading != undefined) {
            this.events.publish('ShowErrorAlert', message.Heading, message.Description);
        } else if (message._body != undefined && this.IsJsonString(message._body)) {
            message = JSON.parse(message._body);
            this.events.publish('ShowErrorAlert', message.Heading, message.Description);
        } else {
            console.error(message.toString());
            this.events.publish('ShowErrorAlert', "An error occured", "An error occured, please ensure you have a internet connection and try again");
        }
    }

    IsJsonString(str) {
        try {
            JSON.parse(str);
        } catch (e) {
            return false;
        }
        return true;
    }

    openWebsite(url){
        const browser = this.iab.create(url);
        browser.show();
    }

    navigate() {

        this.start = "";
        this.destination = this.venueData.Location.Latitude + ',' + this.venueData.Location.Longitude;

        let options: LaunchNavigatorOptions = {

            start: this.start,
            app: this.launchNav.APP.GOOGLE_MAPS
        };

        this.launchNav.navigate(this.destination, options).then((success) => {

        }, error => {
            const browser = this.iab.create("https://maps.google.com/");
            browser.show();
        });
    }

    wazeMe() {

        let isAvailable = this.launchNav.isAppAvailable(this.launchNav.APP.WAZE);
        let app: any;

        if (isAvailable) {

            app = this.launchNav.APP.WAZE;

        } else {

            console.warn("Google Maps not available - falling back to user selection");
            app = this.launchNav.APP.USER_SELECT;

        }

        this.start = "";
        this.destination = this.venueData.Location.Latitude + ',' + this.venueData.Location.Longitude;

        let options: LaunchNavigatorOptions = {

            start: this.start,
            app: this.launchNav.APP.WAZE
        };

        this.launchNav.navigate(this.destination, options).then((success) => {

        }, error => {
            const browser = this.iab.create("https://www.waze.com");
            browser.show();
        });
    }

    UberMe() {

        let isAvailable = this.launchNav.isAppAvailable(this.launchNav.APP.UBER);
        let app: any;

        if (isAvailable) {

            app = this.launchNav.APP.UBER;

        } else {

            console.warn("Google Maps not available - falling back to user selection");
            app = this.launchNav.APP.USER_SELECT;

        }

        this.start = "";
        this.destination = this.venueData.Location.Latitude + ',' + this.venueData.Location.Longitude;

        let options: LaunchNavigatorOptions = {

            start: this.start,
            app: app

        };

        this.launchNav.navigate(this.destination, options)
            .then(success => { },
                error => {
                    const browser = this.iab.create("https://www.uber.com");
                    browser.show();
                }

            );
    }

    shareWhatsApp() {
        let loading = this.loadingCtrl.create({
            content: 'Downloading Image'
        });

        loading.present();
        var url = "http://www.liv2nyt.com/event?eventId=" + this.venueData.Id;
        this.socialShareing.shareViaWhatsApp(this.venueData.Name + '\n\n' + this.venueData.About, this.venueData.Image, '\n \nFor further information about the Event above, visit or download the Liv2Nyt App from the Google Play Store. \n \n ' + url + '\n \n ' + "Follow us on Instagram: @liv2nyt \nVisit our website: www.liv2nyt.com").then(() => {
            loading.dismiss();

        }).catch((err) => {
            loading.dismiss();
            console.log(err);
            var error = {
                Heading: "Error loading Whatsapp",
                Description: "An error occured while loading your Whatsapp"
            }
            //this.showError(error);
            const browser = this.iab.create("https://www.whatsapp.com/");
            browser.show()
        });
    }

    shareFacebook() {
        let loading = this.loadingCtrl.create({
            content: 'Downloading Image'
        });

        loading.present();
        var url = "http://www.liv2nyt.com/event?eventId=" + this.venueData.Id;
        this.socialShareing.shareViaFacebook(url + " \n " + this.venueData.Name + '\n \n' + this.venueData.About, this.venueData.Image, '\n \nFor further information about the Event above, visit or download the Liv2Nyt App from the Google Play Store. \n \n ' + url + '\n \n ' + "Follow us on Instagram: @liv2nyt \nVisit our website: www.liv2nyt.com").then(() => {
            loading.dismiss();
        }).catch((err) => {
            loading.dismiss();
            console.log(err);
            var error = {
                Heading: "Error loading Facebook",
                Description: "An error occured while loading your Facebook"
            }
            //this.showError(error);
            const browser = this.iab.create("https://www.facebook.com/");
            browser.show()

        });
    }

    shareInstagram() {
        let loading = this.loadingCtrl.create({
            content: 'Downloading Image'
        });

        loading.present();
        var url = "http://www.liv2nyt.com/event?eventId=" + this.venueData.Id;
        this.socialShareing.shareViaInstagram(url + " \n " + this.venueData.Name + '\n' + this.venueData.About + '\n \nFor further information about the Event above, visit or download the Liv2Nyt App from the Google Play Store. \n \n ' + url + '\n \n ' + "Follow us on Instagram: @liv2nyt \nVisit our website: www.liv2nyt.com", this.venueData.Image).then(() => {
            loading.dismiss();
        }).catch((err) => {
            loading.dismiss();
            console.log(err);
            var error = {
                Heading: "Error loading Instagram",
                Description: "An error occured while loading your Instagram"
            }
            //this.showError(error);
            const browser = this.iab.create("https://www.instagram.com/");
            browser.show()

        });
    }

    checkIn() {
        let loading = this.loadingCtrl.create({
            content: 'Loading Spot...'
        });

        loading.present();

        var checkInRequest = {
            VenueId: this.venueData.Id,
            Latitude: 0,
            Longitude: 0
        };
        //spotDistance.Latitude = +localStorage.getItem('Latitude');
        //spotDistance.Longitude = +localStorage.getItem('Longitude');

        this.userProvider.checkIn(checkInRequest).then((response: any) => {
            debugger;
            loading.dismiss();
            if (response.WasSuccessful) {
                this.checkedIn = true;
                this.events.publish("showToast", "You have successfully checked in", true);
            } else {
                this.showErrorMessage(response)
            }
        })
            .catch(err => {
                debugger;
                loading.dismiss();
                this.showErrorMessage(err)
            });
    }

    checkOutCheck(){
        const confirm = this.alertCtrl.create({
            title: 'Are you sure you would like to check out?',
            buttons: [
                {
                    text: 'No'
                },
                {
                    text: 'Yes',
                    handler: () => {
                        this.checkOut();
                    }
                }
            ]
        });
        confirm.present();
    }

    checkOut() {
        let loading = this.loadingCtrl.create({
            content: 'Loading Spot...'
        });

        loading.present();

        var checkOutRequest = {
            VenueId: this.venueData.Id,
            Latitude: 0,
            Longitude: 0
        };
        //spotDistance.Latitude = +localStorage.getItem('Latitude');
        //spotDistance.Longitude = +localStorage.getItem('Longitude');

        this.userProvider.checkOut(checkOutRequest).then((response: any) => {
            debugger;
            loading.dismiss();
            if (response.WasSuccessful) {
                this.checkedIn = false;
                this.events.publish("showToast", "You have successfully checked out", true);
            } else {
                this.showErrorMessage(response)
            }
        })
            .catch(err => {
                debugger;
                loading.dismiss();
                this.showErrorMessage(err)
            });
    }

    presentAlert(message)
    {
        const confirm = this.alertCtrl.create({
            title: message,
            buttons: [
                {
                    text: 'Ok',
                    handler: () => {

                    }
                }
            ]
        });
        confirm.present();
    }

    operatingHours()
    {
        this.showHours = !this.showHours;
    }

    setWeekDay()
    {
        this.weekday[0] = "Sunday";
        this.weekday[1] = "Monday";
        this.weekday[2] = "Tuesday";
        this.weekday[3] = "Wednesday";
        this.weekday[4] = "Thursday";
        this.weekday[5] = "Friday";
        this.weekday[6] = "Saturday";
    }

    inviteFriends()
    {
        let friendsModal = this.modalCtrl.create(FriendsModal, {venueId: this.navParams.get("venueId")});
        friendsModal.onDidDismiss(showSuccessAlert => {
            // show alert that invites were successfully sent
            if(showSuccessAlert)
            {
                this.presentAlert('Successfully sent invitations');
            }
          });
          friendsModal.present();
    }
}
